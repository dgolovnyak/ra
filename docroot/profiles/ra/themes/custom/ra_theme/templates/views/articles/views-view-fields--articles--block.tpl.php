<?php

/**
 * @file
 * Default simple view template to all the fields as a row.
 *
 * - $view: The view in use.
 * - $fields: an array of $field objects. Each one contains:
 *   - $field->content: The output of the field.
 *   - $field->raw: The raw data for the field, if it exists. This is NOT output safe.
 *   - $field->class: The safe class id to use.
 *   - $field->handler: The Views field handler object controlling this field. Do not use
 *     var_export to dump this object, as it can't handle the recursion.
 *   - $field->inline: Whether or not the field should be inline.
 *   - $field->inline_html: either div or span based on the above flag.
 *   - $field->wrapper_prefix: A complete wrapper containing the inline_html to use.
 *   - $field->wrapper_suffix: The closing tag for the wrapper.
 *   - $field->separator: an optional separator that may appear before a field.
 *   - $field->label: The wrap label text to use.
 *   - $field->label_html: The full HTML of the label to use including
 *     configured element type.
 * - $row: The raw result object from the query, with all data it fetched.
 *
 * @ingroup views_templates
 */
?>

<?php
  print theme('ra_alters_article', array('fields' => $fields, 'field_name' => 'field_image'));
?>

<div class="popular-label">
  <?php
    print theme('ra_alters_article', array('fields' => $fields, 'field_name' => 'nothing'));
  ?>
</div>

<div class="blue-corner">
  <?php
    print theme('ra_alters_article', array('fields' => $fields, 'field_name' => 'name'));
    print theme('ra_alters_article', array('fields' => $fields, 'field_name' => 'field_icon'));
  ?>
</div>

<div class="overlay">
  <?php
    print theme('ra_alters_article', array('fields' => $fields, 'field_name' => 'title'));
  ?>
  <div class="overlay-dark">
    <?php
      print theme('ra_alters_article', array('fields' => $fields, 'field_name' => 'field_expert'));
    ?>
    <?php
      print theme('ra_alters_article', array('fields' => $fields, 'field_name' => 'value'));
    ?>
  </div>
</div>
