<?php

/**
 * @file
 * Process theme data.
 *
 * Use this file to run your theme specific implimentations of theme functions,
 * such preprocess, process, alters, and theme function overrides.
 *
 * Preprocess and process functions are used to modify or create variables for
 * templates and theme functions. They are a common theming tool in Drupal, often
 * used as an alternative to directly editing or adding code to templates. Its
 * worth spending some time to learn more about these functions - they are a
 * powerful way to easily modify the output of any template variable.
 *
 * Preprocess and Process Functions SEE: http://drupal.org/node/254940#variables-processor
 * 1. Rename each function and instance of "ra_theme" to match
 *    your subthemes name, e.g. if your theme name is "footheme" then the function
 *    name will be "footheme_preprocess_hook". Tip - you can search/replace
 *    on "ra_theme".
 * 2. Uncomment the required function to use.
 */

/**
 * Retheming topic menu link.
 *
 * Adding active-trail class to topic link on article page if it has that tid.
 */
function ra_theme_menu_link__menu_topics_menu(&$vars) {
  $link = &$vars['element'];
  $entity = menu_get_object();

  if (isset($entity->nid)) {
    if (isset($entity->field_topic[LANGUAGE_NONE][0]['tid'])) {
      $href = arg(NULL, $link['#href']);
      if ('taxonomy' == $href[0] && 'term' == $href[1]) {
        if ($entity->field_topic[LANGUAGE_NONE][0]['tid'] == $href[2]) {
          if (!isset($link['#localized_options']['attributes']['class'])) {
            $link['#localized_options']['attributes']['class'] = array();
          }

          $link['#localized_options']['attributes']['class'] += array(
            'active',
            'active-trail',
          );
        }
      }
    }
  }

  return theme('menu_link', $vars);
}

/**
 * Theme override for theme_preprocess_views_view_unformatted().
 */
function ra_theme_preprocess_views_view_unformatted(&$vars) {
  if ('topic_articles' == $vars['view']->name
    && 'block' == $vars['view']->current_display) {
    $count = 0;
    foreach ($vars['rows'] as $id => $row) {
      $class = $count++ < 3 ? 'views-row-three-first' : 'views-row-three-after';
      $vars['classes'][$id][] = $class;

      if (isset($vars['classes'][$id])) {
        $vars['classes_array'][$id] = implode(' ', $vars['classes'][$id]);
      }
      else {
        $vars['classes_array'][$id] = '';
      }
    }
  }
}

/**
 * Override item_list theme.
 */
function ra_theme_item_list(&$vars) {
  if ('ul.slides' == $vars['type']) {
    $vars['type'] = 'ul';
    $vars['attributes'] = array('class' => 'slides');
  }

  return theme_item_list($vars);
}

/**
 * Expert fields patterns support.
 */
function ra_theme_preprocess_views_view_fields(&$vars) {
  if ($vars['view']->name == 'experts') {
    $supported_view_fields = array(
      'field_expert_raconnection',
      'field_expert_occupation',
      'field_expert_location',
      'nothing',
    );

    $tags = array(
      '/\[link:([0-9]+):([^\]]+)\]/i' => 'ra_alters_render_tag_link',
      '/\[html:([a-z]+):([^\]]+)\]/i' => 'ra_alters_render_tag_html',
    );

    foreach ($supported_view_fields as $field_name) {
      if (isset($vars['fields'][$field_name])) {
        $content =& $vars['fields'][$field_name]->content;

        foreach ($tags as $pattern => $func) {
          if (preg_match_all($pattern, $content, $matches, PREG_SET_ORDER)) {
            foreach ($matches as $match) {
              if (function_exists($func)) {
                $replacement = call_user_func($func, $match);
                $content = str_replace($match[0], $replacement, $content);
              }
            }
          }
        }
      }
    }
  }
}

/**
 * Rewrite view individual fields.
 */
function ra_theme_preprocess_views_view_field(&$vars) {
  // Rewrite inspiration plain date field
  if ($vars['view']->name == 'inspirations') {
    if ($vars['field']->field == 'field_publish_date') {
      $time = $vars['output'];

      $vars['output'] = date('l', $time)
        . '<span> ' . date('F j', $time) . '</span>';
    }
  }
}

/**
 * Override Theme function for an single results row.
 */
function ra_theme_pollfield_row($variables) {
  // Rewrite poll results to show only percent value
  $percent_only = explode(' ', $variables['percent_string']);
  $variables['percent_string'] = $percent_only[0];

  // Mark the user's vote.
  $row_class = $variables['user_choice'] ? ' pollfield-chosen' : '';
  $output  = '<div class="pollfield-row' . $row_class . '">';
  $output .= '<div class="text">' . check_plain($variables['choice']) . '</div>';
  $output .= '<div class="barcell">';
  $output .= '<div class="bar"><div class="pollfield-foreground" style="width: ' . $variables['percent'] . '%;"></div></div>';
  $output .= '</div>';
  $output .= '<div class="pollfield-percent">' . $variables['percent_string'] . '</div>';
  $output .= '</div>';
  return $output;
}

/**
 * Override Default field formatter function.
 */
function ra_theme_pollfield_default_formatter($elements) {
  // Node object.
  $node = $elements['elements']['entity'];
  $delta = $elements['elements']['delta'];
  // Get the available choices for this poll.
  $choices = unserialize($elements['elements']['item']['choice']);
  $item = $elements['elements']['item'];

  $output = '';
  if (!empty($choices)) {
    $pollfield_title = check_plain($elements['elements']['item']['question']);
    $poll_result = pollfield_build_results($elements['elements']);

    // Build voting form.
    $form = drupal_get_form('pollfield_voting_form', $elements['elements']);

    $output  = '<div class="pollfield">';
    $output .= '<div id="pollfield-' . $node->nid . '-' . $delta . '" class="pollfield-form">';
    $output .= '<div class="pollfield-title">' . $pollfield_title . '</div>';
    $output .= $poll_result;
    $output .= render($form);
    $output .= '</div>';
    $output .= '</div>';
    $output .= l(t('Back to Poll >'), $GLOBALS['base_url'] . '/#', array(
      'attributes' => array('class' => 'poll-back'),
    ));
  }

  return $output;
}
