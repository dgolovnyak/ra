(function($) {
  var RA = {
    settings: {
      slider: {
        '.slider-default.flexslider': {
          animation: 'slide'
        },
        '.slider-joints.flexslider': {
          animation: "slide",
          prevText: "Previous Slide",
          nextText: "Next Slide",
          start: function(slider) {
            RA.callbacks.callSliderPager(slider, 'init');
          },
          after: function(slider) {
            RA.callbacks.callSliderPager(slider, 'slide');
          }
        }
      }
    },
    callbacks: {
      callSliderPager: function(slider, action) {
        if (typeof slider.container !== 'undefined') {
          $.each(slider.container, function(index, container) {
            var $container = $(container)
              , wrapper_numpager = $container.parents('div.view').find('.numpager')
              , wrapper_slide_title = $container.parents('div.view').find('.slide-title')
              , header_left = $container.find('.flex-active-slide h2').text()
              , header_right = Drupal.t('Slide')
                + ' ' + (slider.currentSlide + 1) + '/' + slider.pagingCount;
            if (action == 'init') {
              $(slider.context).find('.flex-direction-nav')
                .clone(true)
                .appendTo(slider.context)
                .addClass('flex-bottom-nav');
            }
            wrapper_numpager.html(header_right);
            wrapper_slide_title.html(header_left);
          });
        }
      },

      callPledgeAssign: function() {
        $("a.pledge-share").click(function() {
          return !window.open(pledge_fshare_url, "Facebook", "width=640,height=300");
        });
      },

      callMenuHover: function() {
        $('.menu li').hover(function() {
          $(this).find('>a').toggleClass('hover');
        }, function() {
          $(this).find('>a').toggleClass('hover');
        });
      },

      callInitSlider: function() {
        $.each(RA.settings.slider, function(key, settings) {
          var target = $(key);
          target.flexslider(settings);
        });
      },

      callInitOverlay: function() {
        var overlay_top_offset = parseInt($('.overlay-white').css('top'), 10);
        $(window).scroll(function() {
          var overlay_offset = overlay_top_offset - $('body').scrollTop();
          $('.overlay-white').css('top', overlay_offset > 0 ? overlay_offset : 0);
        });
      },

      callSearchTrigger: function() {
        var target = $('div#mini-panel-header_menus div.pane-views.pane-articles')
          , button = $('li.menu-item-1232')
          ;
        if ('searchPopupFirstCall' in Drupal) {
          Drupal.searchPopupFirstCall = false;
        }
        else {
          Drupal.searchPopupFirstCall = true;
        }
        Drupal.searchPopupFirstCall && $('.view-empty', target).hide();
        button.add('.view-display-id-search_articles a.button-close').unbind('click').click(function() {
          button.add('span', button).toggleClass('active');
          target.is(':visible') ? target.hide() : target.show().find('input[name="body_value"]').focus();
          return false;
        });
      },

      callBodyPager: function() {
        var body_texts = $(".node-type-article .field-name-field-article-body");
        if (body_texts.length
          && body_texts.find('.field-items .field-item').length > 1) {
          body_texts.append('<div class="pager_wrapper">'
            + '<div class="info_text"></div>'
            + '<div class="page_navigation"></div></div>');
          body_texts.pajinate({
            items_per_page : 1,
            item_container_id : '.field-items',
            show_first_last : false,
            nav_label_prev: 'Previous',
            nav_order: ["first", "prev", "num", "next", "last"],
            nav_label_info: '{3} of {4}'
          });
          $('.page_navigation a').click(function() {
            $('html, body').animate({
              scrollTop: $(".pane-node-field-article-body").offset().top
            }, 200);
          });
        }
      },

      callAnimateSignup: function() {
        var signup_block = $('div.signup-block.animate');
        if (signup_block.length) {
          window.setTimeout(function() {
            var img_background_x = signup_block.data('background-x')
              , btn_position_y = signup_block.data('bottom-y')
              , delay;
            if ($.browser.msie  && parseInt($.browser.version, 10) <= 8) {
              delay = 50;
              signup_block.css({'background-position': img_background_x + 'px 100%'});
            }
            else {
              delay = 1000;
              signup_block.animate({'background-position': img_background_x + 'px 100%'}
                , delay);
            }
            window.setTimeout(function() {
              signup_block.find('.signup-description').fadeIn('slow', function() {
                signup_block.find('.signup-button a')
                  .animate({'bottom': btn_position_y + 'px'}, 600, function() {
                    var fadeout_timeout = signup_block.data('fadeout');
                    if (fadeout_timeout) {
                      window.setTimeout(function() {
                        signup_block.fadeOut();
                      }, fadeout_timeout);
                    }
                  });
              });
            }, delay);
          }, 500);
        }
        $('.signup-block').find('.signup-close-btn a').click(function() {
          $(this).parents('.signup-block').fadeOut();
        });
      },

      callScrollCustom: function() {
        $('.nano > div').addClass('content');
        $('.nano').nanoScroller({ alwaysVisible: true });
      },

      callCustomForms: function() {
        jQuery(".webform-client-form input[type='radio'][value=0]").attr("checked",true);
        $('.webform-client-form').find('input, textarea, select, button').uniform();
      },

      callMobileDeviceFixes: function() {
        // Add notouch class to recipe menu items
        $('.pane-menu-menu-recipe-menu .menu-depth-1').addClass('notouch');

        if ($.browser.mobile) {
          $('.notouch').removeClass('notouch').click(function() {
            $(this).toggleClass('hover');
          });
        }
      },

      callPlaceholderFix: function() {
        $('input, textarea').placeholder();
      },

      callInspirationSocial: function() {
        $('a.insp-share-all').click(function(e) {
           e.preventDefault();
          $('.social-wrapper').slideToggle();
        });
      },

      callCtoolsModalResize: function() {
        $('div.ctools-modal-content').css({
          'min-height': $('div.ctools-modal-content').css('height')
        });
        $('div.ctools-modal-content').css('height', '');

        $('div.ctools-modal-content .modal-content').css({
          'min-height': $('div.ctools-modal-content .modal-content').css('height')
        });
        $('div.ctools-modal-content .modal-content').css('height', '');
      },

      callFixBodyOlList: function() {
        $('.field-type-text-with-summary ol').each(function(i) {
          var index = 0;

          $(this).find('>li').each(function(j) {
            $(this).attr('value', $(this).attr('value') || ++index);
          });
        });
      },

      callPollResults: function() {
        var context = $('.view-polls')
          , row = $('.pollfield-row', context)
          , form = $('#pollfield-voting-form', context)
          , back = $('.poll-back', context)
          , results = $('a.poll-view-results', context)
          ;
        results.click(function(e) {
          e.preventDefault();
          row.css('display', 'table-row');
          form.add(back).toggle();
        });
        $(back).click(function(e) {
          e.preventDefault();
          row.hide();
          form.add(back).toggle();
        });
        if (!$('.form-item', context).length) {
          row.css('display', 'table-row');
          back.add(results).hide();
        }
      },

      callSetPlaceholderAttribute: function() {
        //sets the attribute "placeholder" on the search form
        //Functional effect: changes <input id="edit-body-value"> to <input id="edit-body-value" placeholder="search">
        //@todo do this with HTML, JS is a a last minute hack
        jQuery('input#edit-body-value').attr('placeholder', 'Search');
      }

      // ,
      // callPaneReorder: function() {
      //   var win_width = $(window).width();
      //   var _sort = function CustomSort(a, b){
      //     return $(a).data(device + '-weight') < $(b).data(device + '-weight');
      //   }
      //   $('body.reorder div').sort(CustomSort).appendTo('#input');

      // }
    }
  }

  $(window).load(function() {
    RA.callbacks.callInitSlider();
  });

  $(function() {
    RA.callbacks.callMobileDeviceFixes();
    RA.callbacks.callAnimateSignup();
    RA.callbacks.callMenuHover();
    RA.callbacks.callCustomForms();
    RA.callbacks.callScrollCustom();
    RA.callbacks.callBodyPager();
    RA.callbacks.callInitOverlay();
    RA.callbacks.callPlaceholderFix();
    RA.callbacks.callFixBodyOlList();
    RA.callbacks.callInspirationSocial();
    RA.callbacks.callPollResults();
    RA.callbacks.callSetPlaceholderAttribute()
  });

  Drupal.behaviors.ra = {
    attach: function(context, settings) {
      RA.callbacks.callPledgeAssign();
      RA.callbacks.callCtoolsModalResize();
      RA.callbacks.callSearchTrigger();
    }
  };
}) (jQuery);
