<?php
/**
 * @file
 * ra_inspiration.features.metatag.inc
 */

/**
 * Implements hook_metatag_export_default().
 */
function ra_inspiration_metatag_export_default() {
  $config = array();

  // Exported Metatag config instance: node:inspiration.
  $config['node:inspiration'] = array(
    'instance' => 'node:inspiration',
    'config' => array(
      'og:image' => array(
        'value' => '[node:field_image_original]',
      ),
    ),
  );

  return $config;
}
