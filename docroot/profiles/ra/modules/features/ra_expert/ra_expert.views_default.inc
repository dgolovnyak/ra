<?php
/**
 * @file
 * ra_expert.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function ra_expert_views_default_views() {
  $export = array();

  $view = new view();
  $view->name = 'experts';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'node';
  $view->human_name = 'Experts';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'Meet the Experts';
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'some';
  $handler->display->display_options['pager']['options']['items_per_page'] = '0';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['style_plugin'] = 'default';
  $handler->display->display_options['row_plugin'] = 'fields';
  /* Field: Content: Image */
  $handler->display->display_options['fields']['field_image']['id'] = 'field_image';
  $handler->display->display_options['fields']['field_image']['table'] = 'field_data_field_image';
  $handler->display->display_options['fields']['field_image']['field'] = 'field_image';
  $handler->display->display_options['fields']['field_image']['label'] = '';
  $handler->display->display_options['fields']['field_image']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_image']['click_sort_column'] = 'fid';
  $handler->display->display_options['fields']['field_image']['settings'] = array(
    'image_style' => 'experts_landing',
    'image_link' => '',
  );
  /* Field: Content: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'node';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['label'] = '';
  $handler->display->display_options['fields']['title']['exclude'] = TRUE;
  $handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
  $handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
  $handler->display->display_options['fields']['title']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['title']['link_to_node'] = FALSE;
  /* Field: Content: Summary line */
  $handler->display->display_options['fields']['field_expert_summary']['id'] = 'field_expert_summary';
  $handler->display->display_options['fields']['field_expert_summary']['table'] = 'field_data_field_expert_summary';
  $handler->display->display_options['fields']['field_expert_summary']['field'] = 'field_expert_summary';
  $handler->display->display_options['fields']['field_expert_summary']['label'] = '';
  $handler->display->display_options['fields']['field_expert_summary']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_expert_summary']['element_label_colon'] = FALSE;
  /* Field: Content: RA Connection */
  $handler->display->display_options['fields']['field_expert_raconnection']['id'] = 'field_expert_raconnection';
  $handler->display->display_options['fields']['field_expert_raconnection']['table'] = 'field_data_field_expert_raconnection';
  $handler->display->display_options['fields']['field_expert_raconnection']['field'] = 'field_expert_raconnection';
  $handler->display->display_options['fields']['field_expert_raconnection']['exclude'] = TRUE;
  /* Field: Content: Occupation */
  $handler->display->display_options['fields']['field_expert_occupation']['id'] = 'field_expert_occupation';
  $handler->display->display_options['fields']['field_expert_occupation']['table'] = 'field_data_field_expert_occupation';
  $handler->display->display_options['fields']['field_expert_occupation']['field'] = 'field_expert_occupation';
  $handler->display->display_options['fields']['field_expert_occupation']['exclude'] = TRUE;
  /* Field: Content: Location */
  $handler->display->display_options['fields']['field_expert_location']['id'] = 'field_expert_location';
  $handler->display->display_options['fields']['field_expert_location']['table'] = 'field_data_field_expert_location';
  $handler->display->display_options['fields']['field_expert_location']['field'] = 'field_expert_location';
  $handler->display->display_options['fields']['field_expert_location']['exclude'] = TRUE;
  /* Field: Content: Short Name */
  $handler->display->display_options['fields']['field_expert_short_name']['id'] = 'field_expert_short_name';
  $handler->display->display_options['fields']['field_expert_short_name']['table'] = 'field_data_field_expert_short_name';
  $handler->display->display_options['fields']['field_expert_short_name']['field'] = 'field_expert_short_name';
  $handler->display->display_options['fields']['field_expert_short_name']['label'] = '';
  $handler->display->display_options['fields']['field_expert_short_name']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_expert_short_name']['alter']['max_length'] = '10';
  $handler->display->display_options['fields']['field_expert_short_name']['alter']['word_boundary'] = FALSE;
  $handler->display->display_options['fields']['field_expert_short_name']['alter']['ellipsis'] = FALSE;
  $handler->display->display_options['fields']['field_expert_short_name']['alter']['trim'] = TRUE;
  $handler->display->display_options['fields']['field_expert_short_name']['element_label_colon'] = FALSE;
  /* Field: View Full Bio link */
  $handler->display->display_options['fields']['title_1']['id'] = 'title_1';
  $handler->display->display_options['fields']['title_1']['table'] = 'node';
  $handler->display->display_options['fields']['title_1']['field'] = 'title';
  $handler->display->display_options['fields']['title_1']['ui_name'] = 'View Full Bio link';
  $handler->display->display_options['fields']['title_1']['label'] = '';
  $handler->display->display_options['fields']['title_1']['exclude'] = TRUE;
  $handler->display->display_options['fields']['title_1']['alter']['alter_text'] = TRUE;
  $handler->display->display_options['fields']['title_1']['alter']['text'] = 'View [field_expert_short_name]\'s Full Bio';
  $handler->display->display_options['fields']['title_1']['element_label_colon'] = FALSE;
  /* Field: Global: Custom text */
  $handler->display->display_options['fields']['nothing']['id'] = 'nothing';
  $handler->display->display_options['fields']['nothing']['table'] = 'views';
  $handler->display->display_options['fields']['nothing']['field'] = 'nothing';
  $handler->display->display_options['fields']['nothing']['label'] = '';
  $handler->display->display_options['fields']['nothing']['alter']['text'] = '<div class="expert-overlay notouch">
  <div class="views-field views-field-title"><span class="field-content">[title]</span></div>
<div class="expert-collapsed">
  <div class="views-field views-field-field-expert-summary">    
    <div class="field-content">[field_expert_summary]</div>
  </div>
</div>
<div class="expert-expanded">
  <div class="views-field views-field-field-expert-raconnection">
    <span class="views-label views-label-field-expert-raconnection">RA Connection: </span>
    <span class="field-content">[field_expert_raconnection]</span>
  </div>
  <div class="views-field views-field-field-expert-occupation">
    <span class="views-label views-label-field-expert-occupation">Occupation: </span>
    <span class="field-content">[field_expert_occupation]</span>
  </div>
  <div class="views-field views-field-field-expert-location">
    <span class="views-label views-label-field-expert-location">Location: </span>
    <span class="field-content">[field_expert_location]</span>
  </div>
  <div class="expert-full-bio">[title_1]</div>
</div>
</div>';
  $handler->display->display_options['fields']['nothing']['element_label_colon'] = FALSE;
  /* Sort criterion: Content: Post date */
  $handler->display->display_options['sorts']['created']['id'] = 'created';
  $handler->display->display_options['sorts']['created']['table'] = 'node';
  $handler->display->display_options['sorts']['created']['field'] = 'created';
  $handler->display->display_options['sorts']['created']['order'] = 'DESC';
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'expert' => 'expert',
  );

  /* Display: Page */
  $handler = $view->new_display('page', 'Page', 'experts_page');
  $handler->display->display_options['defaults']['title'] = FALSE;
  $handler->display->display_options['defaults']['hide_admin_links'] = FALSE;
  $handler->display->display_options['path'] = 'meet-the-experts';

  /* Display: Expert detailed page */
  $handler = $view->new_display('block', 'Expert detailed page', 'expert_detailed');
  $handler->display->display_options['defaults']['pager'] = FALSE;
  $handler->display->display_options['pager']['type'] = 'none';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['defaults']['fields'] = FALSE;
  /* Field: Content: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'node';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['label'] = '';
  $handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
  $handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
  $handler->display->display_options['fields']['title']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['title']['link_to_node'] = FALSE;
  /* Field: Content: RA Connection */
  $handler->display->display_options['fields']['field_expert_raconnection']['id'] = 'field_expert_raconnection';
  $handler->display->display_options['fields']['field_expert_raconnection']['table'] = 'field_data_field_expert_raconnection';
  $handler->display->display_options['fields']['field_expert_raconnection']['field'] = 'field_expert_raconnection';
  $handler->display->display_options['fields']['field_expert_raconnection']['element_type'] = 'span';
  /* Field: Content: Occupation */
  $handler->display->display_options['fields']['field_expert_occupation']['id'] = 'field_expert_occupation';
  $handler->display->display_options['fields']['field_expert_occupation']['table'] = 'field_data_field_expert_occupation';
  $handler->display->display_options['fields']['field_expert_occupation']['field'] = 'field_expert_occupation';
  $handler->display->display_options['fields']['field_expert_occupation']['element_type'] = 'span';
  /* Field: Content: Location */
  $handler->display->display_options['fields']['field_expert_location']['id'] = 'field_expert_location';
  $handler->display->display_options['fields']['field_expert_location']['table'] = 'field_data_field_expert_location';
  $handler->display->display_options['fields']['field_expert_location']['field'] = 'field_expert_location';
  $handler->display->display_options['fields']['field_expert_location']['element_type'] = 'span';
  /* Field: Content: Body */
  $handler->display->display_options['fields']['body']['id'] = 'body';
  $handler->display->display_options['fields']['body']['table'] = 'field_data_body';
  $handler->display->display_options['fields']['body']['field'] = 'body';
  $handler->display->display_options['fields']['body']['label'] = '';
  $handler->display->display_options['fields']['body']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['body']['delta_offset'] = '0';
  $handler->display->display_options['defaults']['arguments'] = FALSE;
  /* Contextual filter: Content: Nid */
  $handler->display->display_options['arguments']['nid']['id'] = 'nid';
  $handler->display->display_options['arguments']['nid']['table'] = 'node';
  $handler->display->display_options['arguments']['nid']['field'] = 'nid';
  $handler->display->display_options['arguments']['nid']['default_action'] = 'default';
  $handler->display->display_options['arguments']['nid']['default_argument_type'] = 'node';
  $handler->display->display_options['arguments']['nid']['summary']['number_of_records'] = '0';
  $handler->display->display_options['arguments']['nid']['summary']['format'] = 'default_summary';
  $handler->display->display_options['arguments']['nid']['summary_options']['items_per_page'] = '25';

  /* Display: Topic Expert Block */
  $handler = $view->new_display('block', 'Topic Expert Block', 'expert_topic');
  $handler->display->display_options['defaults']['title'] = FALSE;
  $handler->display->display_options['defaults']['query'] = FALSE;
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['defaults']['pager'] = FALSE;
  $handler->display->display_options['pager']['type'] = 'some';
  $handler->display->display_options['pager']['options']['items_per_page'] = '1';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['defaults']['relationships'] = FALSE;
  /* Relationship: Content: Votes */
  $handler->display->display_options['relationships']['votingapi_vote']['id'] = 'votingapi_vote';
  $handler->display->display_options['relationships']['votingapi_vote']['table'] = 'node';
  $handler->display->display_options['relationships']['votingapi_vote']['field'] = 'votingapi_vote';
  $handler->display->display_options['relationships']['votingapi_vote']['votingapi'] = array(
    'value_type' => 'points',
    'tag' => 'vote',
  );
  $handler->display->display_options['relationships']['votingapi_vote']['current_user'] = 0;
  $handler->display->display_options['defaults']['fields'] = FALSE;
  /* Field: Content: Path */
  $handler->display->display_options['fields']['path']['id'] = 'path';
  $handler->display->display_options['fields']['path']['table'] = 'node';
  $handler->display->display_options['fields']['path']['field'] = 'path';
  $handler->display->display_options['fields']['path']['label'] = '';
  $handler->display->display_options['fields']['path']['exclude'] = TRUE;
  $handler->display->display_options['fields']['path']['alter']['text'] = 'Meet the Experts';
  $handler->display->display_options['fields']['path']['alter']['path'] = '[path]';
  $handler->display->display_options['fields']['path']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['path']['absolute'] = TRUE;
  /* Field: Content: Image */
  $handler->display->display_options['fields']['field_image']['id'] = 'field_image';
  $handler->display->display_options['fields']['field_image']['table'] = 'field_data_field_image';
  $handler->display->display_options['fields']['field_image']['field'] = 'field_image';
  $handler->display->display_options['fields']['field_image']['label'] = '';
  $handler->display->display_options['fields']['field_image']['alter']['make_link'] = TRUE;
  $handler->display->display_options['fields']['field_image']['alter']['path'] = '[path]';
  $handler->display->display_options['fields']['field_image']['alter']['absolute'] = TRUE;
  $handler->display->display_options['fields']['field_image']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_image']['click_sort_column'] = 'fid';
  $handler->display->display_options['fields']['field_image']['settings'] = array(
    'image_style' => 'expert_topic',
    'image_link' => '',
  );
  /* Field: Content: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'node';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['label'] = '';
  $handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
  $handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
  $handler->display->display_options['fields']['title']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['title']['link_to_node'] = FALSE;
  /* Field: Votes: Value */
  $handler->display->display_options['fields']['value']['id'] = 'value';
  $handler->display->display_options['fields']['value']['table'] = 'votingapi_vote';
  $handler->display->display_options['fields']['value']['field'] = 'value';
  $handler->display->display_options['fields']['value']['relationship'] = 'votingapi_vote';
  $handler->display->display_options['fields']['value']['label'] = '';
  $handler->display->display_options['fields']['value']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['value']['precision'] = '0';
  $handler->display->display_options['fields']['value']['separator'] = '';
  $handler->display->display_options['fields']['value']['appearance'] = 'rate_views_widget';
  /* Field: Global: Custom text */
  $handler->display->display_options['fields']['nothing']['id'] = 'nothing';
  $handler->display->display_options['fields']['nothing']['table'] = 'views';
  $handler->display->display_options['fields']['nothing']['field'] = 'nothing';
  $handler->display->display_options['fields']['nothing']['label'] = '';
  $handler->display->display_options['fields']['nothing']['alter']['text'] = 'Meet the Experts';
  $handler->display->display_options['fields']['nothing']['alter']['make_link'] = TRUE;
  $handler->display->display_options['fields']['nothing']['alter']['path'] = '[path]';
  $handler->display->display_options['fields']['nothing']['alter']['absolute'] = TRUE;
  $handler->display->display_options['fields']['nothing']['element_label_colon'] = FALSE;
  $handler->display->display_options['defaults']['sorts'] = FALSE;
  /* Sort criterion: Global: Random */
  $handler->display->display_options['sorts']['random']['id'] = 'random';
  $handler->display->display_options['sorts']['random']['table'] = 'views';
  $handler->display->display_options['sorts']['random']['field'] = 'random';
  $handler->display->display_options['defaults']['arguments'] = FALSE;
  /* Contextual filter: Content: Topic (field_expert_topic) */
  $handler->display->display_options['arguments']['field_expert_topic_tid']['id'] = 'field_expert_topic_tid';
  $handler->display->display_options['arguments']['field_expert_topic_tid']['table'] = 'field_data_field_expert_topic';
  $handler->display->display_options['arguments']['field_expert_topic_tid']['field'] = 'field_expert_topic_tid';
  $handler->display->display_options['arguments']['field_expert_topic_tid']['default_action'] = 'default';
  $handler->display->display_options['arguments']['field_expert_topic_tid']['default_argument_type'] = 'taxonomy_tid';
  $handler->display->display_options['arguments']['field_expert_topic_tid']['summary']['number_of_records'] = '0';
  $handler->display->display_options['arguments']['field_expert_topic_tid']['summary']['format'] = 'default_summary';
  $handler->display->display_options['arguments']['field_expert_topic_tid']['summary_options']['items_per_page'] = '25';
  $export['experts'] = $view;

  return $export;
}
