<?php
/**
 * @file
 * ra_webform.features.uuid_node.inc
 */

/**
 * Implements hook_uuid_features_default_content().
 */
function ra_webform_uuid_features_default_content() {
  $nodes = array();

  $nodes[] = array(
  'uid' => '1',
  'title' => 'Sign Up',
  'log' => '',
  'status' => '1',
  'comment' => '1',
  'promote' => '0',
  'sticky' => '0',
  'vuuid' => 'a046b5b2-09e3-4e2b-8411-a23db5c385fe',
  'type' => 'webform',
  'language' => 'und',
  'created' => '1365153632',
  'tnid' => '0',
  'translate' => '0',
  'uuid' => '554962ee-2ff8-40a5-a7ec-d0159f1cec66',
  'revision_uid' => '1',
  'body' => array(
    'und' => array(
      0 => array(
        'value' => '<p><strong>Sign Up</strong></p>
<div>Please fill out the form to get:</div>
<div>&bull; Our one-of-a-kind treatment tracking journal&nbsp;</div>
<div>&bull; Exclusive tips and advice</div>
<div>&bull; Treatment information&nbsp;</div>
<div>All fields must be completed unless marked as optional.</div>
',
        'summary' => '',
        'format' => 'full_html',
        'safe_value' => '<p><strong>Sign Up</strong></p>
<div>Please fill out the form to get:</div>
<div>• Our one-of-a-kind treatment tracking journal </div>
<div>• Exclusive tips and advice</div>
<div>• Treatment information </div>
<div>All fields must be completed unless marked as optional.</div>
',
        'safe_summary' => '',
      ),
    ),
  ),
  'field_webform_extra' => array(
    'und' => array(
      0 => array(
        'value' => '<p><strong>WHAT IS RITUXAN?</strong></p>
<p>Rituxan&reg; (rituximab) is a prescription medicine used in adults with another prescription medicine called methotrexate, to reduce the signs and symptoms of moderate to severe active RA, after at least one other medicine called a tumor necrosis factor (TNF) inhibitor has been used and did not work well enough.</p>
<p><strong>IMPORTANT SAFETY INFORMATION</strong></p>
<p>Rituxan can result in serious side effects, some of which could be life threatening. These include:</p>
<ul>
	<li>infusion reactions</li>
	<li>tumor lysis syndrome (TLS)</li>
	<li>severe skin and mouth reactions</li>
	<li>progressive multifocal leukoencephalopathy (PML)</li>
</ul>
<p>Other serious, potentially life-threatening side effects are:</p>
<ul>
	<li>hepatitis B infection that may become active again</li>
	<li>serious infections</li>
	<li>heart problems</li>
	<li>low blood cell counts</li>
</ul>
<p>Common side effects include infections and infusion reactions. Before treatment with Rituxan, patients should tell their doctor if they have an infection, including one that will not go away or that keeps coming back. If patients experience any symptoms or side effects during or after Rituxan treatment, they should seek immediate medical attention. These are not all of the possible side effects with Rituxan. Tell your doctor about any side effect that bothers you or that does not go away. Please read the Rituxan full Prescribing Information, including the Medication Guide http://www.gene.com/gene/products/information/pdf/rituxan-prescribing.pdf . If you have any questions about this information, be sure to discuss them with your doctor.The people featured on this site are members of the RISE Ambassador program, which is sponsored by Genentech. and Biogen Idec Inc. Genentech compensates the Ambassadors for their time and expense while presenting their stories.</p>
<p><strong>INDICATION</strong></p>
<p>ACTEMRA is a prescription medicine called an interleukin-6 (IL-6) receptor inhibitor. ACTEMRA is used to treat adults with moderately to severely active rheumatoid arthritis (RA) after at least one other medicine called a disease modifying antirheumatic drug (DMARD) has been used and did not work well.</p>
<p><strong>IMPORTANT SIDE EFFECT INFORMATION</strong></p>
<p><u>Serious Infections</u></p>
<ul>
	<li>ACTEMRA is a medicine that affects your immune system</li>
	<li>ACTEMRA can lower the ability of your immune system to fight infections</li>
	<li>Some people have serious infections while taking ACTEMRA, including tuberculosis (TB) and infections caused by bacteria, fungi, or viruses that can spread throughout the body</li>
	<li>Some people have died from these infections</li>
	<li>Your doctor should test you for TB before starting ACTEMRA</li>
	<li>Your doctor should monitor you closely for signs and symptoms of TB during treatment with ACTEMRA</li>
</ul>
<p>You should not start taking ACTEMRA if you have any kind of infection unless your healthcare provider says it is okay.</p>
<p>Before starting ACTEMRA, tell your healthcare provider if you:</p>
<ul>
	<li>Think you have an infection or have symptoms of an infection such as fever, sweating, or chills; muscle aches; cough; shortness of breath; blood in phlegm; weight loss; warm, red, or painful skin or sores on your body; diarrhea or stomach pain; burning when you urinate or urinating more often than normal; feel very tired</li>
	<li>Are being treated for an infection</li>
	<li>Get a lot of infections or have infections that keep coming back</li>
	<li>Have diabetes, HIV, or a weak immune system</li>
	<li>Have TB or have been in close contact with someone with TB</li>
	<li>Live or have lived or have traveled to parts of the country, such as the Ohio or Mississippi River Valleys or the Southwest, where there is an increased chance for getting certain kinds of fungal infections</li>
	<li>Have or have had hepatitis B</li>
</ul>
<p>After starting ACTEMRA, call your healthcare provider right away if you have any symptoms of an infection.</p>
<p>Patients should not take ACTEMRA if they are allergic to it or any of its ingredients.</p>
<p>Other serious side effects with ACTEMRA include:</p>
<p><u>Tears (Perforation) of the Stomach or Intestine</u></p>
<p>Before taking ACTEMRA, tell your healthcare provider if you have had diverticulitis (inflammation in parts of the large intestine) or ulcers in your stomach or intestines. Some people taking ACTEMRA get tears in their stomach and intestines. This happens most often in people who are also taking nonsteroidal anti-inflammatory drugs (NSAIDs), corticosteroids, or methotrexate. Tell your healthcare provider right away if you have fever and stomach-area pain that does not go away and a change in your bowel habits.</p>
<p><u>Changes in Certain Laboratory Test Results</u></p>
<p>Your healthcare provider should do blood tests before you start receiving ACTEMRA and every 4 to 8 weeks during treatment to check for changes in your blood test results (including low neutrophil count, low platelet count, increase in certain liver function tests, and increase in blood cholesterol levels). You should not receive ACTEMRA if some results are too low or too high. Your healthcare provider may stop your ACTEMRA treatment for a period of time or change the dose of your medicine if needed because of changes in these blood test results.</p>
<p><u>Cancer</u></p>
<p>ACTEMRA may increase your risk of certain cancers by changing the way your immune system works.</p>
<p><u>Hepatitis B Infection</u></p>
<p>If you are a carrier of the hepatitis B virus (a virus that affects the liver), the virus may become active while you use ACTEMRA. Tell your healthcare provider if you have symptoms of a possible hepatitis B infection such as feeling very tired, yellow-looking skin or eyes, little or no appetite, vomiting, clay-colored bowel movements, fevers, chills, stomach discomfort, muscle aches, dark urine, or skin rash.</p>
<p><u>Serious Allergic Reactions</u></p>
<p>Serious allergic reactions, including death, can happen with ACTEMRA. These reactions can happen with any infusion of ACTEMRA, even if they did not occur with an earlier infusion. Tell your healthcare provider right away if you have any sign of a serious allergic reaction including shortness of breath or trouble breathing; skin rash; swelling of the lips, tongue, or face; chest pain; or feeling dizzy or faint.</p>
<p><u>Nervous System Problems</u></p>
<p>Multiple sclerosis has been diagnosed rarely in people who take ACTEMRA. It is not known what effect ACTEMRA may have on some nervous system disorders.</p>
<p>Common side effects with ACTEMRA in patients with RA include upper respiratory tract infections (common cold, sinus infections), headache, and increased blood pressure (hypertension).</p>
<p>Tell your healthcare provider if you have any side effects that bother you or that do not go away.</p>
<p>These are not all of the possible side effects of ACTEMRA. For more information, ask your healthcare provider or pharmacist.</p>
<p>Patients must tell their healthcare provider if they plan to become pregnant or are pregnant. It is not known if ACTEMRA will harm an unborn baby. Genentech has a registry for pregnant women who take ACTEMRA. Patients who are pregnant or become pregnant while taking ACTEMRA must contact the registry at 1-877-311-8972 and talk to their healthcare provider.</p>
<p>Patients must call their healthcare provider for medical advice about any side effects. You may report side effects to the FDA at 1-800-FDA-1088. You may also report side effects to Genentech at 1-888-835-2555.</p>
<p>Please see accompanying full Prescribing Information http://www.gene.com/gene/products/information/actemra/pdf/pi.pdf, including Boxed Warning and Medication Guide http://www.gene.com/gene/products/information/actemra/pdf/actemra_med_guide.pdf, for additional important safety information.</p>
',
        'summary' => '',
        'format' => 'full_html',
        'safe_value' => '<p><strong>WHAT IS RITUXAN?</strong></p>
<p>Rituxan® (rituximab) is a prescription medicine used in adults with another prescription medicine called methotrexate, to reduce the signs and symptoms of moderate to severe active RA, after at least one other medicine called a tumor necrosis factor (TNF) inhibitor has been used and did not work well enough.</p>
<p><strong>IMPORTANT SAFETY INFORMATION</strong></p>
<p>Rituxan can result in serious side effects, some of which could be life threatening. These include:</p>
<ul><li>infusion reactions</li>
<li>tumor lysis syndrome (TLS)</li>
<li>severe skin and mouth reactions</li>
<li>progressive multifocal leukoencephalopathy (PML)</li>
</ul><p>Other serious, potentially life-threatening side effects are:</p>
<ul><li>hepatitis B infection that may become active again</li>
<li>serious infections</li>
<li>heart problems</li>
<li>low blood cell counts</li>
</ul><p>Common side effects include infections and infusion reactions. Before treatment with Rituxan, patients should tell their doctor if they have an infection, including one that will not go away or that keeps coming back. If patients experience any symptoms or side effects during or after Rituxan treatment, they should seek immediate medical attention. These are not all of the possible side effects with Rituxan. Tell your doctor about any side effect that bothers you or that does not go away. Please read the Rituxan full Prescribing Information, including the Medication Guide <a href="http://www.gene.com/gene/products/information/pdf/rituxan-prescribing.pdf">http://www.gene.com/gene/products/information/pdf/rituxan-prescribing.pdf</a> . If you have any questions about this information, be sure to discuss them with your doctor.The people featured on this site are members of the RISE Ambassador program, which is sponsored by Genentech. and Biogen Idec Inc. Genentech compensates the Ambassadors for their time and expense while presenting their stories.</p>
<p><strong>INDICATION</strong></p>
<p>ACTEMRA is a prescription medicine called an interleukin-6 (IL-6) receptor inhibitor. ACTEMRA is used to treat adults with moderately to severely active rheumatoid arthritis (RA) after at least one other medicine called a disease modifying antirheumatic drug (DMARD) has been used and did not work well.</p>
<p><strong>IMPORTANT SIDE EFFECT INFORMATION</strong></p>
<p><u>Serious Infections</u></p>
<ul><li>ACTEMRA is a medicine that affects your immune system</li>
<li>ACTEMRA can lower the ability of your immune system to fight infections</li>
<li>Some people have serious infections while taking ACTEMRA, including tuberculosis (TB) and infections caused by bacteria, fungi, or viruses that can spread throughout the body</li>
<li>Some people have died from these infections</li>
<li>Your doctor should test you for TB before starting ACTEMRA</li>
<li>Your doctor should monitor you closely for signs and symptoms of TB during treatment with ACTEMRA</li>
</ul><p>You should not start taking ACTEMRA if you have any kind of infection unless your healthcare provider says it is okay.</p>
<p>Before starting ACTEMRA, tell your healthcare provider if you:</p>
<ul><li>Think you have an infection or have symptoms of an infection such as fever, sweating, or chills; muscle aches; cough; shortness of breath; blood in phlegm; weight loss; warm, red, or painful skin or sores on your body; diarrhea or stomach pain; burning when you urinate or urinating more often than normal; feel very tired</li>
<li>Are being treated for an infection</li>
<li>Get a lot of infections or have infections that keep coming back</li>
<li>Have diabetes, HIV, or a weak immune system</li>
<li>Have TB or have been in close contact with someone with TB</li>
<li>Live or have lived or have traveled to parts of the country, such as the Ohio or Mississippi River Valleys or the Southwest, where there is an increased chance for getting certain kinds of fungal infections</li>
<li>Have or have had hepatitis B</li>
</ul><p>After starting ACTEMRA, call your healthcare provider right away if you have any symptoms of an infection.</p>
<p>Patients should not take ACTEMRA if they are allergic to it or any of its ingredients.</p>
<p>Other serious side effects with ACTEMRA include:</p>
<p><u>Tears (Perforation) of the Stomach or Intestine</u></p>
<p>Before taking ACTEMRA, tell your healthcare provider if you have had diverticulitis (inflammation in parts of the large intestine) or ulcers in your stomach or intestines. Some people taking ACTEMRA get tears in their stomach and intestines. This happens most often in people who are also taking nonsteroidal anti-inflammatory drugs (NSAIDs), corticosteroids, or methotrexate. Tell your healthcare provider right away if you have fever and stomach-area pain that does not go away and a change in your bowel habits.</p>
<p><u>Changes in Certain Laboratory Test Results</u></p>
<p>Your healthcare provider should do blood tests before you start receiving ACTEMRA and every 4 to 8 weeks during treatment to check for changes in your blood test results (including low neutrophil count, low platelet count, increase in certain liver function tests, and increase in blood cholesterol levels). You should not receive ACTEMRA if some results are too low or too high. Your healthcare provider may stop your ACTEMRA treatment for a period of time or change the dose of your medicine if needed because of changes in these blood test results.</p>
<p><u>Cancer</u></p>
<p>ACTEMRA may increase your risk of certain cancers by changing the way your immune system works.</p>
<p><u>Hepatitis B Infection</u></p>
<p>If you are a carrier of the hepatitis B virus (a virus that affects the liver), the virus may become active while you use ACTEMRA. Tell your healthcare provider if you have symptoms of a possible hepatitis B infection such as feeling very tired, yellow-looking skin or eyes, little or no appetite, vomiting, clay-colored bowel movements, fevers, chills, stomach discomfort, muscle aches, dark urine, or skin rash.</p>
<p><u>Serious Allergic Reactions</u></p>
<p>Serious allergic reactions, including death, can happen with ACTEMRA. These reactions can happen with any infusion of ACTEMRA, even if they did not occur with an earlier infusion. Tell your healthcare provider right away if you have any sign of a serious allergic reaction including shortness of breath or trouble breathing; skin rash; swelling of the lips, tongue, or face; chest pain; or feeling dizzy or faint.</p>
<p><u>Nervous System Problems</u></p>
<p>Multiple sclerosis has been diagnosed rarely in people who take ACTEMRA. It is not known what effect ACTEMRA may have on some nervous system disorders.</p>
<p>Common side effects with ACTEMRA in patients with RA include upper respiratory tract infections (common cold, sinus infections), headache, and increased blood pressure (hypertension).</p>
<p>Tell your healthcare provider if you have any side effects that bother you or that do not go away.</p>
<p>These are not all of the possible side effects of ACTEMRA. For more information, ask your healthcare provider or pharmacist.</p>
<p>Patients must tell their healthcare provider if they plan to become pregnant or are pregnant. It is not known if ACTEMRA will harm an unborn baby. Genentech has a registry for pregnant women who take ACTEMRA. Patients who are pregnant or become pregnant while taking ACTEMRA must contact the registry at 1-877-311-8972 and talk to their healthcare provider.</p>
<p>Patients must call their healthcare provider for medical advice about any side effects. You may report side effects to the FDA at 1-800-FDA-1088. You may also report side effects to Genentech at 1-888-835-2555.</p>
<p>Please see accompanying full Prescribing Information <a href="http://www.gene.com/gene/products/information/actemra/pdf/pi.pdf">http://www.gene.com/gene/products/information/actemra/pdf/pi.pdf</a>, including Boxed Warning and Medication Guide <a href="http://www.gene.com/gene/products/information/actemra/pdf/actemra_med_guide.pdf">http://www.gene.com/gene/products/information/actemra/pdf/actemra_med_gu...</a>, for additional important safety information.</p>
',
        'safe_summary' => '',
      ),
    ),
  ),
  'rdf_mapping' => array(
    'rdftype' => array(
      0 => 'sioc:Item',
      1 => 'foaf:Document',
    ),
    'title' => array(
      'predicates' => array(
        0 => 'dc:title',
      ),
    ),
    'created' => array(
      'predicates' => array(
        0 => 'dc:date',
        1 => 'dc:created',
      ),
      'datatype' => 'xsd:dateTime',
      'callback' => 'date_iso8601',
    ),
    'changed' => array(
      'predicates' => array(
        0 => 'dc:modified',
      ),
      'datatype' => 'xsd:dateTime',
      'callback' => 'date_iso8601',
    ),
    'body' => array(
      'predicates' => array(
        0 => 'content:encoded',
      ),
    ),
    'uid' => array(
      'predicates' => array(
        0 => 'sioc:has_creator',
      ),
      'type' => 'rel',
    ),
    'name' => array(
      'predicates' => array(
        0 => 'foaf:name',
      ),
    ),
    'comment_count' => array(
      'predicates' => array(
        0 => 'sioc:num_replies',
      ),
      'datatype' => 'xsd:integer',
    ),
    'last_activity' => array(
      'predicates' => array(
        0 => 'sioc:last_activity_date',
      ),
      'datatype' => 'xsd:dateTime',
      'callback' => 'date_iso8601',
    ),
  ),
  'webform' => array(
    'nid' => '486',
    'confirmation' => '',
    'confirmation_format' => NULL,
    'redirect_url' => '<confirmation>',
    'status' => '1',
    'block' => '0',
    'teaser' => '0',
    'allow_draft' => '0',
    'auto_save' => '0',
    'submit_notice' => '1',
    'submit_text' => '',
    'submit_limit' => '-1',
    'submit_interval' => '-1',
    'total_submit_limit' => '-1',
    'total_submit_interval' => '-1',
    'record_exists' => TRUE,
    'roles' => array(
      0 => '1',
      1 => '2',
    ),
    'emails' => array(),
    'components' => array(
      1 => array(
        'nid' => 486,
        'cid' => '1',
        'pid' => '0',
        'form_key' => 'content_information',
        'name' => 'Contact Information',
        'type' => 'fieldset',
        'value' => '',
        'extra' => array(
          'title_display' => 0,
          'private' => 0,
          'collapsible' => 0,
          'collapsed' => 0,
          'conditional_operator' => '=',
          'description' => '',
          'conditional_component' => '',
          'conditional_values' => '',
        ),
        'mandatory' => '0',
        'weight' => '0',
        'page_num' => 1,
      ),
      2 => array(
        'nid' => 486,
        'cid' => '2',
        'pid' => '1',
        'form_key' => 'first_name',
        'name' => 'First Name',
        'type' => 'textfield',
        'value' => '',
        'extra' => array(
          'title_display' => 'before',
          'private' => 0,
          'disabled' => 0,
          'unique' => 0,
          'conditional_operator' => '=',
          'width' => '',
          'maxlength' => '',
          'field_prefix' => '',
          'field_suffix' => '',
          'description' => '',
          'attributes' => array(),
          'conditional_component' => '',
          'conditional_values' => '',
        ),
        'mandatory' => '1',
        'weight' => '1',
        'page_num' => 1,
      ),
      10 => array(
        'nid' => 486,
        'cid' => '10',
        'pid' => '1',
        'form_key' => 'street_address_1',
        'name' => 'Street Address 1',
        'type' => 'textfield',
        'value' => '',
        'extra' => array(
          'title_display' => 'before',
          'private' => 0,
          'disabled' => 0,
          'unique' => 0,
          'conditional_operator' => '=',
          'width' => '',
          'maxlength' => '',
          'field_prefix' => '',
          'field_suffix' => '',
          'description' => '',
          'attributes' => array(),
          'conditional_component' => '',
          'conditional_values' => '',
        ),
        'mandatory' => '0',
        'weight' => '2',
        'page_num' => 1,
      ),
      3 => array(
        'nid' => 486,
        'cid' => '3',
        'pid' => '1',
        'form_key' => 'last_name',
        'name' => 'Last Name',
        'type' => 'textfield',
        'value' => '',
        'extra' => array(
          'title_display' => 'before',
          'private' => 0,
          'disabled' => 0,
          'unique' => 0,
          'conditional_operator' => '=',
          'width' => '',
          'maxlength' => '',
          'field_prefix' => '',
          'field_suffix' => '',
          'description' => '',
          'attributes' => array(),
          'conditional_component' => '',
          'conditional_values' => '',
        ),
        'mandatory' => '1',
        'weight' => '3',
        'page_num' => 1,
      ),
      11 => array(
        'nid' => 486,
        'cid' => '11',
        'pid' => '1',
        'form_key' => 'street_address_2',
        'name' => 'Street Address 2 (Optional)',
        'type' => 'textfield',
        'value' => '',
        'extra' => array(
          'title_display' => 'before',
          'private' => 0,
          'disabled' => 0,
          'unique' => 0,
          'conditional_operator' => '=',
          'width' => '',
          'maxlength' => '',
          'field_prefix' => '',
          'field_suffix' => '',
          'description' => '',
          'attributes' => array(),
          'conditional_component' => '',
          'conditional_values' => '',
        ),
        'mandatory' => '0',
        'weight' => '4',
        'page_num' => 1,
      ),
      14 => array(
        'nid' => 486,
        'cid' => '14',
        'pid' => '1',
        'form_key' => 'year_of_birth',
        'name' => 'Year of Birth',
        'type' => 'select',
        'value' => '',
        'extra' => array(
          'description' => 'You must be 18 years or older to receive information.',
          'items' => '0|1895
1|1896
2|1897
3|1898
4|1899
5|1900
6|1901
7|1902
8|1903
9|1904
10|1905
11|1906
12|1907
13|1908
14|1909
15|1910
16|1911
17|1912
18|1913
19|1914
20|1915
21|1916
22|1917
23|1918
24|1919
25|1920
26|1921
27|1922
28|1923
29|1924
30|1925
31|1926
32|1927
33|1928
34|1929
35|1930
36|1931
37|1932
38|1933
39|1934
40|1935
41|1936
42|1937
43|1938
44|1939
45|1940
46|1941
47|1942
48|1943
49|1944
50|1945
51|1946
52|1947
53|1948
54|1949
55|1950
56|1951
57|1952
58|1953
59|1954
60|1955
61|1956
62|1957
63|1958
64|1959
65|1960
66|1961
67|1962
68|1963
69|1964
70|1965
71|1966
72|1967
73|1968
74|1969
75|1970
76|1971
77|1972
78|1973
79|1974
80|1975
81|1976
82|1977
83|1978
84|1979
85|1980
86|1981
87|1982
88|1983
89|1984
90|1985
91|1986
92|1987
93|1988
94|1989
95|1990
96|1991
97|1992
98|1993
99|1994
100|1995
',
          'options_source' => 'birthday_years',
          'multiple' => 0,
          'title_display' => 'before',
          'private' => 0,
          'aslist' => 1,
          'optrand' => 0,
          'conditional_operator' => '=',
          'other_option' => NULL,
          'other_text' => 'Other...',
          'custom_keys' => FALSE,
          'conditional_component' => '',
          'conditional_values' => '',
        ),
        'mandatory' => '0',
        'weight' => '5',
        'page_num' => 1,
      ),
      12 => array(
        'nid' => 486,
        'cid' => '12',
        'pid' => '1',
        'form_key' => 'zip',
        'name' => 'Zip',
        'type' => 'number',
        'value' => '',
        'extra' => array(
          'title_display' => 'before',
          'private' => 0,
          'type' => 'textfield',
          'decimals' => '',
          'separator' => ',',
          'point' => '.',
          'unique' => 0,
          'integer' => 1,
          'conditional_operator' => '=',
          'excludezero' => 0,
          'field_prefix' => '',
          'field_suffix' => '',
          'description' => '',
          'attributes' => array(),
          'min' => '',
          'max' => '',
          'step' => '',
          'conditional_component' => '',
          'conditional_values' => '',
        ),
        'mandatory' => '0',
        'weight' => '6',
        'page_num' => 1,
      ),
      13 => array(
        'nid' => 486,
        'cid' => '13',
        'pid' => '1',
        'form_key' => 'email',
        'name' => 'Email',
        'type' => 'email',
        'value' => '',
        'extra' => array(
          'title_display' => 'before',
          'private' => 0,
          'disabled' => 0,
          'unique' => 0,
          'conditional_operator' => '=',
          'width' => '',
          'description' => '',
          'attributes' => array(),
          'conditional_component' => '',
          'conditional_values' => '',
        ),
        'mandatory' => '1',
        'weight' => '7',
        'page_num' => 1,
      ),
      4 => array(
        'nid' => 486,
        'cid' => '4',
        'pid' => '0',
        'form_key' => 'conditional_information',
        'name' => 'Condition Information',
        'type' => 'fieldset',
        'value' => '',
        'extra' => array(
          'title_display' => 0,
          'private' => 0,
          'collapsible' => 0,
          'collapsed' => 0,
          'conditional_operator' => '=',
          'description' => '',
          'conditional_component' => '',
          'conditional_values' => '',
        ),
        'mandatory' => '0',
        'weight' => '1',
        'page_num' => 1,
      ),
      5 => array(
        'nid' => 486,
        'cid' => '5',
        'pid' => '4',
        'form_key' => 'relationship',
        'name' => 'What is your relationship to the person with RA?',
        'type' => 'select',
        'value' => '',
        'extra' => array(
          'items' => '1|I’m the person with RA 
2|I’m a family member / friend / care partner',
          'multiple' => 0,
          'title_display' => 'before',
          'private' => 0,
          'aslist' => 0,
          'optrand' => 0,
          'conditional_operator' => '=',
          'other_option' => NULL,
          'other_text' => 'Other...',
          'description' => '',
          'custom_keys' => FALSE,
          'options_source' => '',
          'conditional_component' => '',
          'conditional_values' => '',
        ),
        'mandatory' => '0',
        'weight' => '2',
        'page_num' => 1,
      ),
      7 => array(
        'nid' => 486,
        'cid' => '7',
        'pid' => '4',
        'form_key' => 'diadnose_type',
        'name' => 'What type of RA have you been diagnosed with?',
        'type' => 'select',
        'value' => '',
        'extra' => array(
          'items' => '1|Mild
2|Moderate
3|Severe',
          'multiple' => 0,
          'title_display' => 'before',
          'private' => 0,
          'aslist' => 0,
          'optrand' => 0,
          'conditional_operator' => '=',
          'other_option' => NULL,
          'other_text' => 'Other...',
          'description' => '',
          'custom_keys' => FALSE,
          'options_source' => '',
          'conditional_component' => '',
          'conditional_values' => '',
        ),
        'mandatory' => '0',
        'weight' => '3',
        'page_num' => 1,
      ),
      6 => array(
        'nid' => 486,
        'cid' => '6',
        'pid' => '4',
        'form_key' => 'current_treatment',
        'name' => 'Which of the following treatments do you currently take or have you taken in the past?',
        'type' => 'grid',
        'value' => '',
        'extra' => array(
          'description' => 'such as: Arava® (leflunomide), Azulfidine® (sulfasalazine),
Imuran® (azathioprine), Plaquenil® (hydroxychloroquine)',
          'options' => '0|Never Taken
1|Currently Taking
2|Taken in the Past',
          'questions' => 'treatment1|Actemra® (tocilizumab)
treatment2|Cimzia® (certolizumab pegol)
treatment3|Enbrel® (etanercept)
treatment4|Humira® (adalimumab)
treatment5|Kineret® (anakinra)
treatment6|Orencia® (abatacept)
treatment7|Remicade® (infliximab)
treatment8|Rituxan® (rituximab)
treatment9|Simponi® (golimumab)
treatment10|Xeljanz® (tofacitinib)
treatment11|Methotrexate
treatment12|Other traditional DMARDs',
          'title_display' => 0,
          'private' => 0,
          'optrand' => 0,
          'qrand' => 0,
          'conditional_operator' => '=',
          'custom_option_keys' => 0,
          'custom_question_keys' => 0,
          'conditional_component' => '',
          'conditional_values' => '',
        ),
        'mandatory' => '0',
        'weight' => '4',
        'page_num' => 1,
      ),
      8 => array(
        'nid' => 486,
        'cid' => '8',
        'pid' => '4',
        'form_key' => 'is_satisfied',
        'name' => 'Are you satisfied with your current treatment?',
        'type' => 'select',
        'value' => '',
        'extra' => array(
          'items' => '1|Yes
2|No',
          'multiple' => 0,
          'title_display' => 'before',
          'private' => 0,
          'aslist' => 0,
          'optrand' => 0,
          'conditional_operator' => '=',
          'other_option' => NULL,
          'other_text' => 'Other...',
          'description' => '',
          'custom_keys' => FALSE,
          'options_source' => '',
          'conditional_component' => '',
          'conditional_values' => '',
        ),
        'mandatory' => '0',
        'weight' => '5',
        'page_num' => 1,
      ),
      9 => array(
        'nid' => 486,
        'cid' => '9',
        'pid' => '4',
        'form_key' => 'more_info',
        'name' => 'I’d like more information from Genentech on:',
        'type' => 'select',
        'value' => '',
        'extra' => array(
          'items' => '1|RA, wellness information, treatment options, and product information
2|Upcoming events to help me live better with RA
3|Participating in market research',
          'multiple' => 1,
          'title_display' => 'before',
          'private' => 0,
          'aslist' => 0,
          'optrand' => 0,
          'conditional_operator' => '=',
          'other_option' => NULL,
          'other_text' => 'Other...',
          'description' => '',
          'custom_keys' => FALSE,
          'options_source' => '',
          'conditional_component' => '',
          'conditional_values' => '',
        ),
        'mandatory' => '0',
        'weight' => '6',
        'page_num' => 1,
      ),
      15 => array(
        'nid' => 486,
        'cid' => '15',
        'pid' => '4',
        'form_key' => 'about_your_privacy',
        'name' => 'About Your Privacy',
        'type' => 'markup',
        'value' => '<p><strong>About our privacy</strong></p>
<p>We understand that protecting the privacy of visitors to our websites is very important and that information about your health is particularly sensitive. Sign up with RA World and receive valuable information that can help you play an active role in your own care and gain a better understanding about RA products from Genentech and important RA topics. We only collect personally identifiable information about you if you choose to give it to us. We do not share any of your personally identifiable information with third parties for their own marketing use unless you explicitly give us permission to do so. Please review our Privacy Statement to learn more about how we collect, use, share, and protect information online.</p>
',
        'extra' => array(
          'conditional_operator' => '=',
          'format' => 'full_html',
          'private' => 0,
          'conditional_component' => '',
          'conditional_values' => '',
        ),
        'mandatory' => '0',
        'weight' => '7',
        'page_num' => 1,
      ),
    ),
  ),
  'cid' => '0',
  'last_comment_name' => NULL,
  'last_comment_uid' => '1',
  'comment_count' => '0',
  'print_display' => 0,
  'print_display_comment' => 0,
  'print_display_urllist' => 0,
  'name' => 'admin',
  'picture' => '0',
  'data' => 'a:1:{s:7:"overlay";i:0;}',
  'print_mail_display' => 0,
  'print_mail_display_comment' => 0,
  'print_mail_display_urllist' => 0,
  'date' => '2013-04-05 05:20:32 -0400',
);
  return $nodes;
}
