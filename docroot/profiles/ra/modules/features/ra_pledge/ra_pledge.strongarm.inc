<?php
/**
 * @file
 * ra_pledge.strongarm.inc
 */

/**
 * Implements hook_strongarm().
 */
function ra_pledge_strongarm() {
  $export = array();

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_anonymous_mpledge';
  $strongarm->value = 0;
  $export['comment_anonymous_mpledge'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_default_mode_mpledge';
  $strongarm->value = 1;
  $export['comment_default_mode_mpledge'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_default_per_page_mpledge';
  $strongarm->value = '50';
  $export['comment_default_per_page_mpledge'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_form_location_mpledge';
  $strongarm->value = 1;
  $export['comment_form_location_mpledge'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_mpledge';
  $strongarm->value = '1';
  $export['comment_mpledge'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_preview_mpledge';
  $strongarm->value = '1';
  $export['comment_preview_mpledge'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_subject_field_mpledge';
  $strongarm->value = 1;
  $export['comment_subject_field_mpledge'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'field_bundle_settings_node__mpledge';
  $strongarm->value = array(
    'view_modes' => array(),
    'extra_fields' => array(
      'form' => array(
        'title' => array(
          'weight' => '0',
        ),
        'path' => array(
          'weight' => '3',
        ),
      ),
      'display' => array(),
    ),
  );
  $export['field_bundle_settings_node__mpledge'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'menu_options_mpledge';
  $strongarm->value = array(
    0 => 'main-menu',
  );
  $export['menu_options_mpledge'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'menu_parent_mpledge';
  $strongarm->value = 'main-menu:0';
  $export['menu_parent_mpledge'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'node_options_mpledge';
  $strongarm->value = array(
    0 => 'status',
  );
  $export['node_options_mpledge'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'node_preview_mpledge';
  $strongarm->value = '1';
  $export['node_preview_mpledge'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'node_submitted_mpledge';
  $strongarm->value = 0;
  $export['node_submitted_mpledge'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'ra_pledge_message_thanks';
  $strongarm->value = '<div class="pledge-thx">Thanks for pledging</div> You are person number <div class="pledge-number">@count</div> to make the pledge.';
  $export['ra_pledge_message_thanks'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'ra_pledge_message_thanks_node';
  $strongarm->value = '<div class="pledge-thx">Thanks for making the Pledge!</div> You are person number <div class="pledge-number">@count</div> to pledge to @pledge. Share this news to inspire others!';
  $export['ra_pledge_message_thanks_node'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'ra_pledge_message_vote';
  $strongarm->value = '<div class="pledge-count"><div>@count</div> people</div> have pledged to @pledge';
  $export['ra_pledge_message_vote'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'ra_pledge_message_vote_node';
  $strongarm->value = '<div class="pledge-count"><div>@count</div> people</div> have pledged to @pledge';
  $export['ra_pledge_message_vote_node'] = $strongarm;

  return $export;
}
