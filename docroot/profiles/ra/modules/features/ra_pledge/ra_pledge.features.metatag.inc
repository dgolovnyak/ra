<?php
/**
 * @file
 * ra_pledge.features.metatag.inc
 */

/**
 * Implements hook_metatag_export_default().
 */
function ra_pledge_metatag_export_default() {
  $config = array();

  // Exported Metatag config instance: node:mpledge.
  $config['node:mpledge'] = array(
    'instance' => 'node:mpledge',
    'config' => array(
      'og:image' => array(
        'value' => '[node:field_image]',
      ),
    ),
  );

  return $config;
}
