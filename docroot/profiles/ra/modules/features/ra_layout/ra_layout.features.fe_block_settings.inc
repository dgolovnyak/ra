<?php
/**
 * @file
 * ra_layout.features.fe_block_settings.inc
 */

/**
 * Implements hook_default_fe_block_settings().
 */
function ra_layout_default_fe_block_settings() {
  $export = array();

  $export['version'] = '2.0';

  $export['panels_mini-footer_menus'] = array(
    'cache' => -1,
    'custom' => '0',
    'delta' => 'footer_menus',
    'module' => 'panels_mini',
    'node_types' => array(),
    'pages' => 'learn-about-2-prescription-treatments-ra-genentech
find-prescription-ra-treatment-right-you
genentech-offers-2-prescription-treatments-ra',
    'roles' => array(),
    'themes' => array(
      'bartik' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'bartik',
        'weight' => '0',
      ),
      'ra_theme' => array(
        'region' => 'footer',
        'status' => '1',
        'theme' => 'ra_theme',
        'weight' => '0',
      ),
      'seven' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'seven',
        'weight' => '0',
      ),
    ),
    'title' => '',
    'visibility' => '0',
  );

  $export['panels_mini-header_menus'] = array(
    'cache' => -1,
    'custom' => '0',
    'delta' => 'header_menus',
    'module' => 'panels_mini',
    'node_types' => array(),
    'pages' => 'learn-about-2-prescription-treatments-ra-genentech
find-prescription-ra-treatment-right-you
genentech-offers-2-prescription-treatments-ra',
    'roles' => array(),
    'themes' => array(
      'bartik' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'bartik',
        'weight' => '0',
      ),
      'ra_theme' => array(
        'region' => 'header',
        'status' => '1',
        'theme' => 'ra_theme',
        'weight' => '-16',
      ),
      'seven' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'seven',
        'weight' => '0',
      ),
    ),
    'title' => '',
    'visibility' => '0',
  );

  return $export;
}
