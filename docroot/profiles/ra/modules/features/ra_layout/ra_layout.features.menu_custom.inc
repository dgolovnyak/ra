<?php
/**
 * @file
 * ra_layout.features.menu_custom.inc
 */

/**
 * Implements hook_menu_default_menu_custom().
 */
function ra_layout_menu_default_menu_custom() {
  $menus = array();

  // Exported menu: menu-copyright-menu.
  $menus['menu-copyright-menu'] = array(
    'menu_name' => 'menu-copyright-menu',
    'title' => 'Copyright menu',
    'description' => '',
  );
  // Exported menu: menu-footer-menu.
  $menus['menu-footer-menu'] = array(
    'menu_name' => 'menu-footer-menu',
    'title' => 'Footer Menu',
    'description' => '',
  );
  // Exported menu: menu-header-links.
  $menus['menu-header-links'] = array(
    'menu_name' => 'menu-header-links',
    'title' => 'Header Links',
    'description' => '',
  );
  // Exported menu: menu-social-links.
  $menus['menu-social-links'] = array(
    'menu_name' => 'menu-social-links',
    'title' => 'Social Links',
    'description' => '',
  );
  // Exported menu: menu-topics-menu.
  $menus['menu-topics-menu'] = array(
    'menu_name' => 'menu-topics-menu',
    'title' => 'Topics Menu',
    'description' => '',
  );
  // Translatables
  // Included for use with string extractors like potx.
  t('Copyright menu');
  t('Footer Menu');
  t('Header Links');
  t('Social Links');
  t('Topics Menu');


  return $menus;
}
