<?php
/**
 * @file
 * ra_recipe.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function ra_recipe_views_default_views() {
  $export = array();

  $view = new view();
  $view->name = 'recipes';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'node';
  $view->human_name = 'Recipes';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = '<none>';
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'infinite_scroll';
  $handler->display->display_options['pager']['options']['items_per_page'] = '12';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['pager']['options']['id'] = '0';
  $handler->display->display_options['pager']['options']['quantity'] = '9';
  $handler->display->display_options['style_plugin'] = 'default';
  $handler->display->display_options['row_plugin'] = 'fields';
  /* Field: Content: Nid */
  $handler->display->display_options['fields']['nid']['id'] = 'nid';
  $handler->display->display_options['fields']['nid']['table'] = 'node';
  $handler->display->display_options['fields']['nid']['field'] = 'nid';
  $handler->display->display_options['fields']['nid']['label'] = '';
  $handler->display->display_options['fields']['nid']['exclude'] = TRUE;
  $handler->display->display_options['fields']['nid']['element_label_colon'] = FALSE;
  /* Field: Content: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'node';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['label'] = '';
  $handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
  $handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
  /* Field: Content: Image */
  $handler->display->display_options['fields']['field_image']['id'] = 'field_image';
  $handler->display->display_options['fields']['field_image']['table'] = 'field_data_field_image';
  $handler->display->display_options['fields']['field_image']['field'] = 'field_image';
  $handler->display->display_options['fields']['field_image']['label'] = '';
  $handler->display->display_options['fields']['field_image']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_image']['click_sort_column'] = 'fid';
  $handler->display->display_options['fields']['field_image']['settings'] = array(
    'image_style' => 'recipes_landing',
    'image_link' => '',
  );
  /* Sort criterion: Content: Post date */
  $handler->display->display_options['sorts']['created']['id'] = 'created';
  $handler->display->display_options['sorts']['created']['table'] = 'node';
  $handler->display->display_options['sorts']['created']['field'] = 'created';
  $handler->display->display_options['sorts']['created']['order'] = 'DESC';
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'recipe' => 'recipe',
  );

  /* Display: Recipes Landing page */
  $handler = $view->new_display('page', 'Recipes Landing page', 'recipes_page');
  $handler->display->display_options['defaults']['query'] = FALSE;
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['query']['options']['distinct'] = TRUE;
  $handler->display->display_options['defaults']['pager'] = FALSE;
  $handler->display->display_options['pager']['type'] = 'infinite_scroll';
  $handler->display->display_options['pager']['options']['items_per_page'] = '12';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['pager']['options']['id'] = '1';
  $handler->display->display_options['pager']['options']['quantity'] = '9';
  $handler->display->display_options['exposed_block'] = TRUE;
  $handler->display->display_options['defaults']['header'] = FALSE;
  $handler->display->display_options['defaults']['relationships'] = FALSE;
  /* Relationship: Content: Votes */
  $handler->display->display_options['relationships']['votingapi_vote']['id'] = 'votingapi_vote';
  $handler->display->display_options['relationships']['votingapi_vote']['table'] = 'node';
  $handler->display->display_options['relationships']['votingapi_vote']['field'] = 'votingapi_vote';
  $handler->display->display_options['relationships']['votingapi_vote']['votingapi'] = array(
    'value_type' => 'points',
    'tag' => 'vote',
  );
  $handler->display->display_options['relationships']['votingapi_vote']['current_user'] = 0;
  $handler->display->display_options['defaults']['fields'] = FALSE;
  /* Field: Content: Nid */
  $handler->display->display_options['fields']['nid']['id'] = 'nid';
  $handler->display->display_options['fields']['nid']['table'] = 'node';
  $handler->display->display_options['fields']['nid']['field'] = 'nid';
  $handler->display->display_options['fields']['nid']['label'] = '';
  $handler->display->display_options['fields']['nid']['exclude'] = TRUE;
  $handler->display->display_options['fields']['nid']['element_label_colon'] = FALSE;
  /* Field: Content: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'node';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['label'] = '';
  $handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
  $handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
  /* Field: Content: Image */
  $handler->display->display_options['fields']['field_image']['id'] = 'field_image';
  $handler->display->display_options['fields']['field_image']['table'] = 'field_data_field_image';
  $handler->display->display_options['fields']['field_image']['field'] = 'field_image';
  $handler->display->display_options['fields']['field_image']['label'] = '';
  $handler->display->display_options['fields']['field_image']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_image']['click_sort_column'] = 'fid';
  $handler->display->display_options['fields']['field_image']['settings'] = array(
    'image_style' => 'recipes_landing',
    'image_link' => '',
  );
  /* Field: Content: Expert Name */
  $handler->display->display_options['fields']['field_expert']['id'] = 'field_expert';
  $handler->display->display_options['fields']['field_expert']['table'] = 'field_data_field_expert';
  $handler->display->display_options['fields']['field_expert']['field'] = 'field_expert';
  $handler->display->display_options['fields']['field_expert']['label'] = '';
  $handler->display->display_options['fields']['field_expert']['alter']['alter_text'] = TRUE;
  $handler->display->display_options['fields']['field_expert']['alter']['text'] = 'by [field_expert]';
  $handler->display->display_options['fields']['field_expert']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_expert']['type'] = 'node_reference_plain';
  /* Field: Votes: Value */
  $handler->display->display_options['fields']['value']['id'] = 'value';
  $handler->display->display_options['fields']['value']['table'] = 'votingapi_vote';
  $handler->display->display_options['fields']['value']['field'] = 'value';
  $handler->display->display_options['fields']['value']['relationship'] = 'votingapi_vote';
  $handler->display->display_options['fields']['value']['label'] = '';
  $handler->display->display_options['fields']['value']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['value']['precision'] = '0';
  $handler->display->display_options['fields']['value']['separator'] = '';
  $handler->display->display_options['fields']['value']['appearance'] = 'rate_views_widget';
  $handler->display->display_options['defaults']['filter_groups'] = FALSE;
  $handler->display->display_options['defaults']['filters'] = FALSE;
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'recipe' => 'recipe',
  );
  /* Filter criterion: Content: Body (body) */
  $handler->display->display_options['filters']['body_value']['id'] = 'body_value';
  $handler->display->display_options['filters']['body_value']['table'] = 'field_data_body';
  $handler->display->display_options['filters']['body_value']['field'] = 'body_value';
  $handler->display->display_options['filters']['body_value']['operator'] = 'contains';
  $handler->display->display_options['filters']['body_value']['exposed'] = TRUE;
  $handler->display->display_options['filters']['body_value']['expose']['operator_id'] = 'body_value_op';
  $handler->display->display_options['filters']['body_value']['expose']['label'] = 'Search';
  $handler->display->display_options['filters']['body_value']['expose']['operator'] = 'body_value_op';
  $handler->display->display_options['filters']['body_value']['expose']['identifier'] = 'body_value';
  $handler->display->display_options['filters']['body_value']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    3 => 0,
  );
  /* Filter criterion: Content: Dishes &amp; Meals (field_recipe_dishes_meals) */
  $handler->display->display_options['filters']['field_recipe_dishes_meals_tid']['id'] = 'field_recipe_dishes_meals_tid';
  $handler->display->display_options['filters']['field_recipe_dishes_meals_tid']['table'] = 'field_data_field_recipe_dishes_meals';
  $handler->display->display_options['filters']['field_recipe_dishes_meals_tid']['field'] = 'field_recipe_dishes_meals_tid';
  $handler->display->display_options['filters']['field_recipe_dishes_meals_tid']['exposed'] = TRUE;
  $handler->display->display_options['filters']['field_recipe_dishes_meals_tid']['expose']['operator_id'] = 'field_recipe_dishes_meals_tid_op';
  $handler->display->display_options['filters']['field_recipe_dishes_meals_tid']['expose']['label'] = 'Dishes & Meals';
  $handler->display->display_options['filters']['field_recipe_dishes_meals_tid']['expose']['operator'] = 'field_recipe_dishes_meals_tid_op';
  $handler->display->display_options['filters']['field_recipe_dishes_meals_tid']['expose']['identifier'] = 'dishes_meals';
  $handler->display->display_options['filters']['field_recipe_dishes_meals_tid']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    3 => 0,
  );
  $handler->display->display_options['filters']['field_recipe_dishes_meals_tid']['type'] = 'select';
  $handler->display->display_options['filters']['field_recipe_dishes_meals_tid']['vocabulary'] = 'dishes_meals';
  /* Filter criterion: Content: Main ingredient (field_recipe_main_ingredient) */
  $handler->display->display_options['filters']['field_recipe_main_ingredient_tid']['id'] = 'field_recipe_main_ingredient_tid';
  $handler->display->display_options['filters']['field_recipe_main_ingredient_tid']['table'] = 'field_data_field_recipe_main_ingredient';
  $handler->display->display_options['filters']['field_recipe_main_ingredient_tid']['field'] = 'field_recipe_main_ingredient_tid';
  $handler->display->display_options['filters']['field_recipe_main_ingredient_tid']['exposed'] = TRUE;
  $handler->display->display_options['filters']['field_recipe_main_ingredient_tid']['expose']['operator_id'] = 'field_recipe_main_ingredient_tid_op';
  $handler->display->display_options['filters']['field_recipe_main_ingredient_tid']['expose']['label'] = 'Main ingredient';
  $handler->display->display_options['filters']['field_recipe_main_ingredient_tid']['expose']['operator'] = 'field_recipe_main_ingredient_tid_op';
  $handler->display->display_options['filters']['field_recipe_main_ingredient_tid']['expose']['identifier'] = 'main_ingredient';
  $handler->display->display_options['filters']['field_recipe_main_ingredient_tid']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    3 => 0,
  );
  $handler->display->display_options['filters']['field_recipe_main_ingredient_tid']['type'] = 'select';
  $handler->display->display_options['filters']['field_recipe_main_ingredient_tid']['vocabulary'] = 'main_ingredient';
  /* Filter criterion: Content: RA-Friendly Category (field_recipe_friendly_category) */
  $handler->display->display_options['filters']['field_recipe_friendly_category_tid']['id'] = 'field_recipe_friendly_category_tid';
  $handler->display->display_options['filters']['field_recipe_friendly_category_tid']['table'] = 'field_data_field_recipe_friendly_category';
  $handler->display->display_options['filters']['field_recipe_friendly_category_tid']['field'] = 'field_recipe_friendly_category_tid';
  $handler->display->display_options['filters']['field_recipe_friendly_category_tid']['exposed'] = TRUE;
  $handler->display->display_options['filters']['field_recipe_friendly_category_tid']['expose']['operator_id'] = 'field_recipe_friendly_category_tid_op';
  $handler->display->display_options['filters']['field_recipe_friendly_category_tid']['expose']['label'] = 'RA-Friendly Category';
  $handler->display->display_options['filters']['field_recipe_friendly_category_tid']['expose']['operator'] = 'field_recipe_friendly_category_tid_op';
  $handler->display->display_options['filters']['field_recipe_friendly_category_tid']['expose']['identifier'] = 'ra_friendly_category';
  $handler->display->display_options['filters']['field_recipe_friendly_category_tid']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    3 => 0,
  );
  $handler->display->display_options['filters']['field_recipe_friendly_category_tid']['type'] = 'select';
  $handler->display->display_options['filters']['field_recipe_friendly_category_tid']['vocabulary'] = 'ra_friendly_category';
  $handler->display->display_options['path'] = 'recipes';

  /* Display: Similar Recipes */
  $handler = $view->new_display('block', 'Similar Recipes', 'similar_recipes');
  $handler->display->display_options['defaults']['title'] = FALSE;
  $handler->display->display_options['title'] = 'Similar Recipes';
  $handler->display->display_options['defaults']['query'] = FALSE;
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['query']['options']['distinct'] = TRUE;
  $handler->display->display_options['defaults']['pager'] = FALSE;
  $handler->display->display_options['pager']['type'] = 'some';
  $handler->display->display_options['pager']['options']['items_per_page'] = '4';
  $handler->display->display_options['defaults']['footer'] = FALSE;
  $handler->display->display_options['defaults']['empty'] = FALSE;
  $handler->display->display_options['defaults']['relationships'] = FALSE;
  /* Relationship: Content: Votes */
  $handler->display->display_options['relationships']['votingapi_vote']['id'] = 'votingapi_vote';
  $handler->display->display_options['relationships']['votingapi_vote']['table'] = 'node';
  $handler->display->display_options['relationships']['votingapi_vote']['field'] = 'votingapi_vote';
  $handler->display->display_options['relationships']['votingapi_vote']['votingapi'] = array(
    'value_type' => 'points',
    'tag' => 'vote',
  );
  $handler->display->display_options['relationships']['votingapi_vote']['current_user'] = 0;
  /* Relationship: Content: Similar Recipes (field_recipe_similar_recipes) - reverse */
  $handler->display->display_options['relationships']['reverse_field_recipe_similar_recipes_node']['id'] = 'reverse_field_recipe_similar_recipes_node';
  $handler->display->display_options['relationships']['reverse_field_recipe_similar_recipes_node']['table'] = 'node';
  $handler->display->display_options['relationships']['reverse_field_recipe_similar_recipes_node']['field'] = 'reverse_field_recipe_similar_recipes_node';
  $handler->display->display_options['relationships']['reverse_field_recipe_similar_recipes_node']['required'] = TRUE;
  $handler->display->display_options['defaults']['fields'] = FALSE;
  /* Field: Content: Nid */
  $handler->display->display_options['fields']['nid_1']['id'] = 'nid_1';
  $handler->display->display_options['fields']['nid_1']['table'] = 'node';
  $handler->display->display_options['fields']['nid_1']['field'] = 'nid';
  $handler->display->display_options['fields']['nid_1']['label'] = '';
  $handler->display->display_options['fields']['nid_1']['exclude'] = TRUE;
  $handler->display->display_options['fields']['nid_1']['element_label_colon'] = FALSE;
  /* Field: Content: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'node';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['label'] = '';
  $handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
  $handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
  $handler->display->display_options['fields']['title']['element_label_colon'] = FALSE;
  /* Field: Content: Image */
  $handler->display->display_options['fields']['field_image']['id'] = 'field_image';
  $handler->display->display_options['fields']['field_image']['table'] = 'field_data_field_image';
  $handler->display->display_options['fields']['field_image']['field'] = 'field_image';
  $handler->display->display_options['fields']['field_image']['label'] = '';
  $handler->display->display_options['fields']['field_image']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_image']['click_sort_column'] = 'fid';
  $handler->display->display_options['fields']['field_image']['settings'] = array(
    'image_style' => 'recipes_similar',
    'image_link' => '',
  );
  /* Field: Content: Expert Name */
  $handler->display->display_options['fields']['field_expert']['id'] = 'field_expert';
  $handler->display->display_options['fields']['field_expert']['table'] = 'field_data_field_expert';
  $handler->display->display_options['fields']['field_expert']['field'] = 'field_expert';
  $handler->display->display_options['fields']['field_expert']['label'] = '';
  $handler->display->display_options['fields']['field_expert']['alter']['alter_text'] = TRUE;
  $handler->display->display_options['fields']['field_expert']['alter']['text'] = 'by [field_expert]';
  $handler->display->display_options['fields']['field_expert']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_expert']['type'] = 'node_reference_plain';
  /* Field: Content: Type */
  $handler->display->display_options['fields']['type']['id'] = 'type';
  $handler->display->display_options['fields']['type']['table'] = 'node';
  $handler->display->display_options['fields']['type']['field'] = 'type';
  $handler->display->display_options['fields']['type']['label'] = '';
  $handler->display->display_options['fields']['type']['exclude'] = TRUE;
  $handler->display->display_options['fields']['type']['element_label_colon'] = FALSE;
  /* Field: Votes: Value */
  $handler->display->display_options['fields']['value']['id'] = 'value';
  $handler->display->display_options['fields']['value']['table'] = 'votingapi_vote';
  $handler->display->display_options['fields']['value']['field'] = 'value';
  $handler->display->display_options['fields']['value']['relationship'] = 'votingapi_vote';
  $handler->display->display_options['fields']['value']['label'] = '';
  $handler->display->display_options['fields']['value']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['value']['precision'] = '0';
  $handler->display->display_options['fields']['value']['separator'] = '';
  $handler->display->display_options['fields']['value']['appearance'] = 'rate_views_widget';
  /* Field: Content: Nid */
  $handler->display->display_options['fields']['nid']['id'] = 'nid';
  $handler->display->display_options['fields']['nid']['table'] = 'node';
  $handler->display->display_options['fields']['nid']['field'] = 'nid';
  $handler->display->display_options['fields']['nid']['relationship'] = 'reverse_field_recipe_similar_recipes_node';
  $handler->display->display_options['fields']['nid']['label'] = '';
  $handler->display->display_options['fields']['nid']['exclude'] = TRUE;
  $handler->display->display_options['fields']['nid']['element_label_colon'] = FALSE;
  $handler->display->display_options['defaults']['arguments'] = FALSE;
  /* Contextual filter: Content: Nid */
  $handler->display->display_options['arguments']['nid']['id'] = 'nid';
  $handler->display->display_options['arguments']['nid']['table'] = 'node';
  $handler->display->display_options['arguments']['nid']['field'] = 'nid';
  $handler->display->display_options['arguments']['nid']['relationship'] = 'reverse_field_recipe_similar_recipes_node';
  $handler->display->display_options['arguments']['nid']['default_action'] = 'default';
  $handler->display->display_options['arguments']['nid']['default_argument_type'] = 'node';
  $handler->display->display_options['arguments']['nid']['summary']['number_of_records'] = '0';
  $handler->display->display_options['arguments']['nid']['summary']['format'] = 'default_summary';
  $handler->display->display_options['arguments']['nid']['summary_options']['items_per_page'] = '25';
  $export['recipes'] = $view;

  return $export;
}
