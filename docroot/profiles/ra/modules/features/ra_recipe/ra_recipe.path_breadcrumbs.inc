<?php
/**
 * @file
 * ra_recipe.path_breadcrumbs.inc
 */

/**
 * Implements hook_path_breadcrumbs_settings_info().
 */
function ra_recipe_path_breadcrumbs_settings_info() {
  $export = array();

  $path_breadcrumb = new stdClass();
  $path_breadcrumb->api_version = 1;
  $path_breadcrumb->machine_name = 'recipe_breadcrumb';
  $path_breadcrumb->name = 'Recipe Breadcrumb';
  $path_breadcrumb->path = 'node/%node';
  $path_breadcrumb->data = array(
    'titles' => array(
      0 => '%node:field_topic',
      1 => 'The RA-Friendly Virtual Cookbook',
      2 => '!page_title',
    ),
    'paths' => array(
      0 => 'taxonomy/term/%node:field_topic:tid',
      1 => 'recipes',
      2 => '<none>',
    ),
    'home' => 1,
    'translatable' => 0,
    'arguments' => array(
      'node' => array(
        'position' => 1,
        'argument' => 'entity_id:node',
        'settings' => array(
          'identifier' => 'Node: ID',
        ),
      ),
    ),
    'access' => array(
      'plugins' => array(
        0 => array(
          'name' => 'node_type',
          'settings' => array(
            'type' => array(
              'recipe' => 'recipe',
            ),
          ),
          'context' => 'node',
          'not' => FALSE,
        ),
      ),
      'logic' => 'and',
    ),
  );
  $path_breadcrumb->weight = 0;
  $export['recipe_breadcrumb'] = $path_breadcrumb;

  $path_breadcrumb = new stdClass();
  $path_breadcrumb->api_version = 1;
  $path_breadcrumb->machine_name = 'recipe_landing';
  $path_breadcrumb->name = 'Recipe Landing';
  $path_breadcrumb->path = 'recipes';
  $path_breadcrumb->data = array(
    'titles' => array(
      0 => 'Diet & Exercise',
      1 => 'The RA-Friendly Virtual Cookbook',
    ),
    'paths' => array(
      0 => 'taxonomy/term/2',
      1 => '<none>',
    ),
    'home' => 1,
    'translatable' => 0,
    'arguments' => NULL,
    'access' => array(),
  );
  $path_breadcrumb->weight = 0;
  $export['recipe_landing'] = $path_breadcrumb;

  return $export;
}
