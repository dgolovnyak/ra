<?php
/**
 * @file
 * ra_basic_page.features.uuid_node.inc
 */

/**
 * Implements hook_uuid_features_default_content().
 */
function ra_basic_page_uuid_features_default_content() {
  $nodes = array();

  $nodes[] = array(
  'uid' => '1',
  'title' => 'Contact Us',
  'log' => '',
  'status' => '1',
  'comment' => '1',
  'promote' => '0',
  'sticky' => '0',
  'vuuid' => 'e0321f1f-4ee8-4beb-98c5-7f094cad4174',
  'type' => 'page',
  'language' => 'und',
  'created' => '1366300830',
  'tnid' => '0',
  'translate' => '0',
  'uuid' => '0645f766-7346-446f-84e5-4e85da16f7a6',
  'revision_uid' => '1',
  'body' => array(
    'und' => array(
      0 => array(
        'value' => '<p>Maecenas at dolor mi, et posuere nibh. Integer eget sem auctor lectus bibendum aliquet. Donec metus dolor, varius in tristique eget, faucibus quis quam. Pellentesque vehicula blandit ipsum sit amet euismod. Aenean pulvinar, felis vel hendrerit sodales, felis justo posuere risus, sed tincidunt neque augue ut eros. Mauris lacinia molestie elit, vel pretium nisl cursus in. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos.</p>
<p>Integer et tristique odio. Maecenas vestibulum pellentesque venenatis. Aliquam volutpat dolor in urna venenatis a lacinia lacus congue. Quisque eget leo a turpis porta placerat vel non lectus. Sed posuere nisi augue, ornare ullamcorper neque. Aliquam tellus lacus, egestas at suscipit non, tempus eu enim. Pellentesque nec velit justo. Sed congue leo pretium turpis pretium nec venenatis quam accumsan. Ut imperdiet sodales nibh, et ultricies est malesuada sed. Sed malesuada iaculis lobortis. Integer vel lectus neque, nec pellentesque elit. Sed bibendum sollicitudin aliquam. Nullam vitae ligula quis felis posuere ornare.</p>
',
        'summary' => '',
        'format' => 'full_html',
        'safe_value' => '<p>Maecenas at dolor mi, et posuere nibh. Integer eget sem auctor lectus bibendum aliquet. Donec metus dolor, varius in tristique eget, faucibus quis quam. Pellentesque vehicula blandit ipsum sit amet euismod. Aenean pulvinar, felis vel hendrerit sodales, felis justo posuere risus, sed tincidunt neque augue ut eros. Mauris lacinia molestie elit, vel pretium nisl cursus in. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos.</p>
<p>Integer et tristique odio. Maecenas vestibulum pellentesque venenatis. Aliquam volutpat dolor in urna venenatis a lacinia lacus congue. Quisque eget leo a turpis porta placerat vel non lectus. Sed posuere nisi augue, ornare ullamcorper neque. Aliquam tellus lacus, egestas at suscipit non, tempus eu enim. Pellentesque nec velit justo. Sed congue leo pretium turpis pretium nec venenatis quam accumsan. Ut imperdiet sodales nibh, et ultricies est malesuada sed. Sed malesuada iaculis lobortis. Integer vel lectus neque, nec pellentesque elit. Sed bibendum sollicitudin aliquam. Nullam vitae ligula quis felis posuere ornare.</p>
',
        'safe_summary' => '',
      ),
    ),
  ),
  'field_image' => array(),
  'metatags' => array(),
  'rdf_mapping' => array(
    'rdftype' => array(
      0 => 'foaf:Document',
    ),
    'title' => array(
      'predicates' => array(
        0 => 'dc:title',
      ),
    ),
    'created' => array(
      'predicates' => array(
        0 => 'dc:date',
        1 => 'dc:created',
      ),
      'datatype' => 'xsd:dateTime',
      'callback' => 'date_iso8601',
    ),
    'changed' => array(
      'predicates' => array(
        0 => 'dc:modified',
      ),
      'datatype' => 'xsd:dateTime',
      'callback' => 'date_iso8601',
    ),
    'body' => array(
      'predicates' => array(
        0 => 'content:encoded',
      ),
    ),
    'uid' => array(
      'predicates' => array(
        0 => 'sioc:has_creator',
      ),
      'type' => 'rel',
    ),
    'name' => array(
      'predicates' => array(
        0 => 'foaf:name',
      ),
    ),
    'comment_count' => array(
      'predicates' => array(
        0 => 'sioc:num_replies',
      ),
      'datatype' => 'xsd:integer',
    ),
    'last_activity' => array(
      'predicates' => array(
        0 => 'sioc:last_activity_date',
      ),
      'datatype' => 'xsd:dateTime',
      'callback' => 'date_iso8601',
    ),
  ),
  'cid' => '0',
  'last_comment_name' => NULL,
  'last_comment_uid' => '1',
  'comment_count' => '0',
  'print_display' => 0,
  'print_display_comment' => 0,
  'print_display_urllist' => 0,
  'name' => 'admin',
  'picture' => '0',
  'data' => 'a:1:{s:7:"overlay";i:0;}',
  'print_mail_display' => 0,
  'print_mail_display_comment' => 0,
  'print_mail_display_urllist' => 0,
  'date' => '2013-04-18 12:00:30 -0400',
);
  $nodes[] = array(
  'uid' => '1',
  'title' => 'Unsubscribe',
  'log' => '',
  'status' => '1',
  'comment' => '1',
  'promote' => '0',
  'sticky' => '0',
  'vuuid' => 'f9943105-0972-4b1b-8c30-fd1e9cdb2c96',
  'type' => 'page',
  'language' => 'und',
  'created' => '1366300754',
  'tnid' => '0',
  'translate' => '0',
  'uuid' => '079a591b-774e-4389-b3b9-33412f063964',
  'revision_uid' => '1',
  'body' => array(
    'und' => array(
      0 => array(
        'value' => '<p>Nulla facilisi. Mauris sit amet lorem consectetur orci facilisis volutpat nec id magna. Nam at nunc ac ante vulputate varius. Donec posuere ullamcorper purus ut interdum. Nam fringilla ornare enim, ac accumsan odio dictum vulputate. Sed convallis placerat viverra. Aenean non sem quis mauris ultrices ultrices ut ut odio. Mauris lacinia nulla in augue congue ut consectetur lorem euismod. Aliquam erat volutpat. Praesent a luctus lacus.</p>
<p>Phasellus sapien ipsum, facilisis ac mollis vel, malesuada a lorem. In vulputate dolor vitae nisl accumsan posuere. Curabitur sit amet luctus dolor. Cras auctor viverra scelerisque. Etiam fermentum turpis sed ante scelerisque at feugiat erat scelerisque. Sed vitae sem mi, ac iaculis orci. Proin id diam est. Nam orci augue, scelerisque in bibendum vel, volutpat at urna. Sed lacinia, nisl quis tempor faucibus, augue neque placerat dui, eget convallis sem nisl et lacus. In porta, neque vitae ornare pharetra, nibh leo mollis neque, in luctus urna enim id massa. Phasellus ultricies mi et dolor sagittis sed imperdiet mauris malesuada. Nulla purus erat, dignissim sed feugiat vel, porta quis nisi. Nullam dictum auctor mauris, at aliquam turpis iaculis in.</p>
',
        'summary' => '',
        'format' => 'full_html',
        'safe_value' => '<p>Nulla facilisi. Mauris sit amet lorem consectetur orci facilisis volutpat nec id magna. Nam at nunc ac ante vulputate varius. Donec posuere ullamcorper purus ut interdum. Nam fringilla ornare enim, ac accumsan odio dictum vulputate. Sed convallis placerat viverra. Aenean non sem quis mauris ultrices ultrices ut ut odio. Mauris lacinia nulla in augue congue ut consectetur lorem euismod. Aliquam erat volutpat. Praesent a luctus lacus.</p>
<p>Phasellus sapien ipsum, facilisis ac mollis vel, malesuada a lorem. In vulputate dolor vitae nisl accumsan posuere. Curabitur sit amet luctus dolor. Cras auctor viverra scelerisque. Etiam fermentum turpis sed ante scelerisque at feugiat erat scelerisque. Sed vitae sem mi, ac iaculis orci. Proin id diam est. Nam orci augue, scelerisque in bibendum vel, volutpat at urna. Sed lacinia, nisl quis tempor faucibus, augue neque placerat dui, eget convallis sem nisl et lacus. In porta, neque vitae ornare pharetra, nibh leo mollis neque, in luctus urna enim id massa. Phasellus ultricies mi et dolor sagittis sed imperdiet mauris malesuada. Nulla purus erat, dignissim sed feugiat vel, porta quis nisi. Nullam dictum auctor mauris, at aliquam turpis iaculis in.</p>
',
        'safe_summary' => '',
      ),
    ),
  ),
  'field_image' => array(),
  'metatags' => array(),
  'rdf_mapping' => array(
    'rdftype' => array(
      0 => 'foaf:Document',
    ),
    'title' => array(
      'predicates' => array(
        0 => 'dc:title',
      ),
    ),
    'created' => array(
      'predicates' => array(
        0 => 'dc:date',
        1 => 'dc:created',
      ),
      'datatype' => 'xsd:dateTime',
      'callback' => 'date_iso8601',
    ),
    'changed' => array(
      'predicates' => array(
        0 => 'dc:modified',
      ),
      'datatype' => 'xsd:dateTime',
      'callback' => 'date_iso8601',
    ),
    'body' => array(
      'predicates' => array(
        0 => 'content:encoded',
      ),
    ),
    'uid' => array(
      'predicates' => array(
        0 => 'sioc:has_creator',
      ),
      'type' => 'rel',
    ),
    'name' => array(
      'predicates' => array(
        0 => 'foaf:name',
      ),
    ),
    'comment_count' => array(
      'predicates' => array(
        0 => 'sioc:num_replies',
      ),
      'datatype' => 'xsd:integer',
    ),
    'last_activity' => array(
      'predicates' => array(
        0 => 'sioc:last_activity_date',
      ),
      'datatype' => 'xsd:dateTime',
      'callback' => 'date_iso8601',
    ),
  ),
  'cid' => '0',
  'last_comment_name' => NULL,
  'last_comment_uid' => '1',
  'comment_count' => '0',
  'print_display' => 0,
  'print_display_comment' => 0,
  'print_display_urllist' => 0,
  'name' => 'admin',
  'picture' => '0',
  'data' => 'a:1:{s:7:"overlay";i:0;}',
  'print_mail_display' => 0,
  'print_mail_display_comment' => 0,
  'print_mail_display_urllist' => 0,
  'date' => '2013-04-18 11:59:14 -0400',
);
  $nodes[] = array(
  'uid' => '1',
  'title' => 'Privacy Policy',
  'log' => '',
  'status' => '1',
  'comment' => '1',
  'promote' => '0',
  'sticky' => '0',
  'vuuid' => '3a7e5563-d71b-4541-9c3b-1418448665ea',
  'type' => 'page',
  'language' => 'und',
  'created' => '1366300593',
  'tnid' => '0',
  'translate' => '0',
  'uuid' => 'd7f751c7-9f6c-44b1-ad76-78d02fad13de',
  'revision_uid' => '1',
  'body' => array(
    'und' => array(
      0 => array(
        'value' => '<p>Pellentesque eu lacus ut enim accumsan volutpat eu non ipsum. Mauris enim mauris, tristique in rhoncus ultricies, semper quis purus. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Vestibulum ornare malesuada lorem non feugiat. Aenean consectetur nulla quis ante rutrum commodo. Maecenas commodo justo accumsan orci euismod sollicitudin. Mauris libero lectus, pulvinar sit amet scelerisque in, sollicitudin at libero.</p>
<p>In magna diam, scelerisque a consectetur in, laoreet quis neque. Nullam eget arcu magna, sed viverra odio. Praesent et arcu non purus tristique dictum. Sed id nunc in urna volutpat vestibulum ut a urna. Etiam eu orci nisi. Duis egestas imperdiet neque, ac laoreet est ultricies eu. Curabitur fringilla, elit ut pharetra elementum, arcu erat elementum neque, id condimentum sem nulla id urna. Maecenas a ligula dolor. Suspendisse mattis semper consequat. Quisque at turpis sit amet dui sagittis imperdiet.</p>
',
        'summary' => '',
        'format' => 'full_html',
        'safe_value' => '<p>Pellentesque eu lacus ut enim accumsan volutpat eu non ipsum. Mauris enim mauris, tristique in rhoncus ultricies, semper quis purus. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Vestibulum ornare malesuada lorem non feugiat. Aenean consectetur nulla quis ante rutrum commodo. Maecenas commodo justo accumsan orci euismod sollicitudin. Mauris libero lectus, pulvinar sit amet scelerisque in, sollicitudin at libero.</p>
<p>In magna diam, scelerisque a consectetur in, laoreet quis neque. Nullam eget arcu magna, sed viverra odio. Praesent et arcu non purus tristique dictum. Sed id nunc in urna volutpat vestibulum ut a urna. Etiam eu orci nisi. Duis egestas imperdiet neque, ac laoreet est ultricies eu. Curabitur fringilla, elit ut pharetra elementum, arcu erat elementum neque, id condimentum sem nulla id urna. Maecenas a ligula dolor. Suspendisse mattis semper consequat. Quisque at turpis sit amet dui sagittis imperdiet.</p>
',
        'safe_summary' => '',
      ),
    ),
  ),
  'field_image' => array(),
  'metatags' => array(),
  'rdf_mapping' => array(
    'rdftype' => array(
      0 => 'foaf:Document',
    ),
    'title' => array(
      'predicates' => array(
        0 => 'dc:title',
      ),
    ),
    'created' => array(
      'predicates' => array(
        0 => 'dc:date',
        1 => 'dc:created',
      ),
      'datatype' => 'xsd:dateTime',
      'callback' => 'date_iso8601',
    ),
    'changed' => array(
      'predicates' => array(
        0 => 'dc:modified',
      ),
      'datatype' => 'xsd:dateTime',
      'callback' => 'date_iso8601',
    ),
    'body' => array(
      'predicates' => array(
        0 => 'content:encoded',
      ),
    ),
    'uid' => array(
      'predicates' => array(
        0 => 'sioc:has_creator',
      ),
      'type' => 'rel',
    ),
    'name' => array(
      'predicates' => array(
        0 => 'foaf:name',
      ),
    ),
    'comment_count' => array(
      'predicates' => array(
        0 => 'sioc:num_replies',
      ),
      'datatype' => 'xsd:integer',
    ),
    'last_activity' => array(
      'predicates' => array(
        0 => 'sioc:last_activity_date',
      ),
      'datatype' => 'xsd:dateTime',
      'callback' => 'date_iso8601',
    ),
  ),
  'cid' => '0',
  'last_comment_name' => NULL,
  'last_comment_uid' => '1',
  'comment_count' => '0',
  'print_display' => 0,
  'print_display_comment' => 0,
  'print_display_urllist' => 0,
  'name' => 'admin',
  'picture' => '0',
  'data' => 'a:1:{s:7:"overlay";i:0;}',
  'print_mail_display' => 0,
  'print_mail_display_comment' => 0,
  'print_mail_display_urllist' => 0,
  'date' => '2013-04-18 11:56:33 -0400',
);
  $nodes[] = array(
  'uid' => '1',
  'title' => 'Terms and Conditions',
  'log' => '',
  'status' => '1',
  'comment' => '1',
  'promote' => '0',
  'sticky' => '0',
  'vuuid' => 'e6b716bd-e03a-4147-8e35-87c5d9a4ec73',
  'type' => 'page',
  'language' => 'und',
  'created' => '1366300706',
  'tnid' => '0',
  'translate' => '0',
  'uuid' => 'f71a2ac4-1867-425a-ba16-f222bb246627',
  'revision_uid' => '1',
  'body' => array(
    'und' => array(
      0 => array(
        'value' => '<p>Mauris porta suscipit sem ac mattis. Suspendisse ullamcorper leo vitae turpis hendrerit at luctus sapien consectetur. Vivamus vitae metus eget sapien volutpat elementum in sed lectus. Integer mauris libero, egestas tempus tempus vel, vulputate sit amet nibh. Cras viverra mollis fermentum. Cras eget purus sed nisl cursus accumsan. Sed hendrerit condimentum pretium. In hac habitasse platea dictumst. Nunc nunc urna, vulputate id posuere sit amet, imperdiet et orci. Quisque semper vulputate ante, sit amet volutpat nulla mollis volutpat. Sed ligula tellus, facilisis ut bibendum eget, posuere at ipsum</p>
<p>Sed massa lectus, facilisis sed dapibus at, pharetra id leo. Nulla condimentum, ipsum sodales venenatis mattis, arcu risus imperdiet ipsum, quis laoreet tortor arcu luctus velit. Suspendisse nec est sapien. Cras dignissim dapibus massa a euismod. Mauris consequat mi vitae mauris feugiat a rutrum odio porttitor. Sed iaculis augue vitae orci aliquet bibendum. Cras porttitor placerat condimentum.</p>
',
        'summary' => '',
        'format' => 'full_html',
        'safe_value' => '<p>Mauris porta suscipit sem ac mattis. Suspendisse ullamcorper leo vitae turpis hendrerit at luctus sapien consectetur. Vivamus vitae metus eget sapien volutpat elementum in sed lectus. Integer mauris libero, egestas tempus tempus vel, vulputate sit amet nibh. Cras viverra mollis fermentum. Cras eget purus sed nisl cursus accumsan. Sed hendrerit condimentum pretium. In hac habitasse platea dictumst. Nunc nunc urna, vulputate id posuere sit amet, imperdiet et orci. Quisque semper vulputate ante, sit amet volutpat nulla mollis volutpat. Sed ligula tellus, facilisis ut bibendum eget, posuere at ipsum</p>
<p>Sed massa lectus, facilisis sed dapibus at, pharetra id leo. Nulla condimentum, ipsum sodales venenatis mattis, arcu risus imperdiet ipsum, quis laoreet tortor arcu luctus velit. Suspendisse nec est sapien. Cras dignissim dapibus massa a euismod. Mauris consequat mi vitae mauris feugiat a rutrum odio porttitor. Sed iaculis augue vitae orci aliquet bibendum. Cras porttitor placerat condimentum.</p>
',
        'safe_summary' => '',
      ),
    ),
  ),
  'field_image' => array(),
  'metatags' => array(),
  'rdf_mapping' => array(
    'rdftype' => array(
      0 => 'foaf:Document',
    ),
    'title' => array(
      'predicates' => array(
        0 => 'dc:title',
      ),
    ),
    'created' => array(
      'predicates' => array(
        0 => 'dc:date',
        1 => 'dc:created',
      ),
      'datatype' => 'xsd:dateTime',
      'callback' => 'date_iso8601',
    ),
    'changed' => array(
      'predicates' => array(
        0 => 'dc:modified',
      ),
      'datatype' => 'xsd:dateTime',
      'callback' => 'date_iso8601',
    ),
    'body' => array(
      'predicates' => array(
        0 => 'content:encoded',
      ),
    ),
    'uid' => array(
      'predicates' => array(
        0 => 'sioc:has_creator',
      ),
      'type' => 'rel',
    ),
    'name' => array(
      'predicates' => array(
        0 => 'foaf:name',
      ),
    ),
    'comment_count' => array(
      'predicates' => array(
        0 => 'sioc:num_replies',
      ),
      'datatype' => 'xsd:integer',
    ),
    'last_activity' => array(
      'predicates' => array(
        0 => 'sioc:last_activity_date',
      ),
      'datatype' => 'xsd:dateTime',
      'callback' => 'date_iso8601',
    ),
  ),
  'cid' => '0',
  'last_comment_name' => NULL,
  'last_comment_uid' => '1',
  'comment_count' => '0',
  'print_display' => 0,
  'print_display_comment' => 0,
  'print_display_urllist' => 0,
  'name' => 'admin',
  'picture' => '0',
  'data' => 'a:1:{s:7:"overlay";i:0;}',
  'print_mail_display' => 0,
  'print_mail_display_comment' => 0,
  'print_mail_display_urllist' => 0,
  'date' => '2013-04-18 11:58:26 -0400',
);
  return $nodes;
}
