<?php
/**
 * @file ra_alters.inline.php.inc
 * TODO: Enter file description here.
 */


/**
 * Custom perm callback for cookbook placeholder.
 *
 * @see Articles view, Expert Cookbook Placeholder display.
 */
function _ra_alters_cookbook_placeholder_enabled() {
  $node = menu_get_object();

  // Check if user is Melinda (:nid = 390) to show Coockbook placeholder.
  if ($node->nid && $node->nid != 390) {
    return FALSE;
  } 

  return TRUE;
}

/**
 * Check if user visited site first time.
 *
 * @see Header Menus mini panel, Sign Up pane.
 */
function _ra_alters_check_anon_first_visit() {
  return user_is_anonymous()
    && (!isset($_COOKIE['Drupal_visitor_first_visited'])
      || $_COOKIE['Drupal_visitor_first_visited']);
}

/**
 * Get HP inspiration social links.
 *
 * @see Inspirations view, Inspiration of the Day, Inspiration Popup displays.
 */
function _ra_alters_hp_inspiration_links($results, $type = 'inspiration_links') {
  $inspiration = reset($results);
  return theme('ra_alters_' . $type
    , array('node' => $inspiration, 'render' => TRUE));
}
