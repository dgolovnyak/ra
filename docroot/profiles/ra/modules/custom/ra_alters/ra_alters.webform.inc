<?php
/**
 * @file ra_alters.webform.inc
 * TODO: Enter file description here.
 */

/**
 * Get birtday years.
 */
function ra_alters_options_birthday_years($component, $flat, $filter, $arguments) {
  $current_year = date('Y') - 18;

  return range($current_year - 60, $current_year);
}
