(function ($) {
  Drupal.behaviors.initModalFormsContact = {
    attach: function (context, settings) {
      $("a.modal").addClass('ctools-use-modal');
      $("a.modal[href*='/printmail'], a.modal[href*='?q=printmail']", context).once('init-modal-forms-contact', function () {
        var pattern = /printmail\/[0-9]+/;
        if (pattern.test(this.href)) {
          this.href = this.href.replace(pattern, 'modal_forms/nojs/' + this.href.match(pattern));
        }
      }).addClass('ctools-use-modal');
    }
  };
})(jQuery);
