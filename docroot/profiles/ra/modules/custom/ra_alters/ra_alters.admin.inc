<?php
/**
 * @file ra_alters.admin.inc
 * TODO: Enter file description here.
 */


/**
 * RA settings form.
 */
function ra_alters_global_settings($form, &$form_state) {
  $form['pledge'] = array(
    '#type' => 'fieldset',
    '#title' => t('Pledge Settings'),
    '#collapsible' => TRUE,
  );

  $form['pledge']['ra_pledge_message_vote'] = array(
    '#title' => t('Pledge vote status message'),
    '#type' => 'textarea',
    '#default_value' => variable_get('ra_pledge_message_vote', ''),
    '#required' => TRUE,
  );

  $form['pledge']['ra_pledge_message_thanks'] = array(
    '#title' => t('Pledge thanks status message'),
    '#type' => 'textarea',
    '#default_value' => variable_get('ra_pledge_message_thanks', ''),
    '#required' => TRUE,
  );

  $form['pledge']['ra_pledge_message_vote_node'] = array(
    '#title' => t('Node pledge vote status message'),
    '#type' => 'textarea',
    '#default_value' => variable_get('ra_pledge_message_vote_node', ''),
    '#required' => TRUE,
  );

  $form['pledge']['ra_pledge_message_thanks_node'] = array(
    '#title' => t('Node pledge thanks status message'),
    '#type' => 'textarea',
    '#default_value' => variable_get('ra_pledge_message_thanks_node', ''),
    '#required' => TRUE,
  );

  return system_settings_form($form);
}
