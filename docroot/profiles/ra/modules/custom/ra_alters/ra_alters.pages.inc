<?php
/**
 * @file ra_alters.pages.inc
 * TODO: Enter file description here.
 */


/**
 * A modal inspiration list callback.
 */
function ra_alters_inspiration_list($js = NULL) {
  $html = views_embed_view('inspirations', 'inspiration_popup');

  // Fall back if $js is not set.
  if (!$js) {
    return $html;
  }

  ctools_include('modal');
  ctools_include('ajax');

  $commands = array();
  $commands[] = ctools_modal_command_display($title, $html);
  print ajax_render($commands);
}

/**
 * A modal sendmail callback.
 */
function ra_alters_printemail_node($js = NULL, $node) {
  // Fall back if $js is not set.
  if (!$js) {
    return drupal_get_form('ra_alters_send_email', $node);
  }

  ctools_include('modal');
  ctools_include('ajax');
  $form_state = array(
    // 'title' => t('Contact'),
    'ajax' => TRUE,
    'build_info' => array(
      'args' => array(
        (object) $node,
      ),
    ),
  );

  $output = ctools_modal_form_wrapper('ra_alters_send_email', $form_state);
  if (!empty($form_state['executed'])) {
    // We'll just overwrite the form output if it was successful.
    $output = array();

    ctools_add_js('ajax-responder');
    if ($form_state['values']['redirect']) {
      $output[] = ctools_ajax_command_redirect('sign-up');
    }
    else {
      $output[] = ctools_ajax_command_reload();
    }
  }
  print ajax_render($output);
}

/**
 * Send mail form.
 */
function ra_alters_send_email($form, $form_state, $node) {
  $form = array();

  $path = 'node/' . $node->nid;
  $path_title = _print_get_title($path);

  $form['path'] = array('#type' => 'value', '#value' => $path);
  $form['title'] = array('#type' => 'value', '#value' => $path_title);

  $form['txt_to_addrs'] = array(
    '#type' => 'textfield',
    '#title' => t('To'),
    '#size' => 62,
    '#required' => TRUE,
    '#placeholder' => t('Recipient\'s e-mail address'),
  );

  $form['fld_from_addr'] = array(
    '#type' => 'textfield',
    '#title' => t('From'),
    '#size' => 62,
    '#required' => TRUE,
    '#placeholder' => t('Your e-mail address'),
  );

  $form['redirect'] = array(
    '#type' => 'checkbox',
    '#title' => t('Check this box to provide additional information and receive more from @site_url.com'
                , array('@site_url' => variable_get('site_name', ''))),
  );

  $form['btn_submit'] = array(
    '#name' => 'submit',
    '#type' => 'submit',
    '#value' => t('Send'),
  );

  return $form;
}

/**
 * Send mail form validate.
 */
function ra_alters_send_email_validate($form, &$form_state) {
  module_load_include('inc', 'print_mail', 'print_mail');
  $values =& $form_state['values'];

  $values += array(
    'fld_from_name' => '',
    'fld_subject'   => $values['title'],
    'txt_message'   => '',
  );

  print_mail_form_validate($form, $form_state);
}

/**
 * Send mail form submit.
 */
function ra_alters_send_email_submit($form, &$form_state) {
  module_load_include('inc', 'print_mail', 'print_mail');
  print_mail_form_submit($form, $form_state);

  if ($form_state['values']['redirect']) {
    $form_state['redirect'] = 'sign-up';
  }
}

