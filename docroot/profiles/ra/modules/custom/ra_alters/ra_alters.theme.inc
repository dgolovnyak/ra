<?php
/**
 * @file ra_alters.theme.inc
 * TODO: Enter file description here.
 */


/**
 * Theme function to output wrapped field.
 */
function theme_ra_alters_article($vars) {
  extract($vars);

  $output = '';
  if (isset($fields[$field_name])) {
    $output .= $fields[$field_name]->wrapper_prefix;
    $output .= $fields[$field_name]->content;
    $output .= $fields[$field_name]->wrapper_suffix;
  }

  return $output;
}

/**
 * Theme function to output social links.
 */
function theme_ra_alters_social_links($vars) {
  extract($vars);

  $block = array();
  if (isset($node) && isset($node->nid)) {
    $node = node_load($node->nid);
    $print_link = array('#markup'
          => l('Print ' . $node->type, 'print/' . $node->nid
              , array('attributes' => array('class' => 'print'))));

    if ($delta == RA_ALTERS_SOCIAL_DELTA) {
      $block = theme('ra_alters_share_links', array('nid' => $node->nid));

      $send_link = array('#markup' => l('Email', 'printmail/' . $node->nid
                   , array('attributes' => array('class' => array('modal'
                      , 'ctools-modal-modal-popup-send-email')))));
      $like_link = array('#markup' => rate_embed($node, 'content_like'));

      if (!$block) {
        $block['content']['social_share'] = array();
      }

      $block['content']['social_share'][] = $send_link;
      $block['content']['social_share'][] = $like_link;

      if ($node->type == 'article') {
        $block['content']['social_share'][] = $print_link;
      }
    }
    else {
      $block['content'][] = $print_link;
    }
  }

  return $render ? drupal_render($block) : $block;
}

/**
 * Theme function to output share links.
 */
function theme_ra_alters_share_links($vars) {
  extract($vars);

  $block = array();
  $sites = variable_get('social_share_sites', array());
  $node = (object) node_load($nid);

  foreach ($sites as $site => $enabled) {
    if ($enabled) {
      $block['content']['social_share'][] = _social_share_link($site, $node);
    }
  }

  return $render ? drupal_render($block) : $block;
}

/**
 * Theme function to output inspiration social links.
 */
function theme_ra_alters_inspiration_links($vars) {
  extract($vars);

  $block = array();
  if (isset($node) && $node->nid) {
    $node = node_load($node->nid);

    $block['content'][] = array('#markup' => rate_embed($node, 'content_like'));
    $block['content'][] =  array('#markup' => l('', '#', array('attributes' => array('class' => 'insp-share-all'))));
    $block['content'][] =  array('#markup' => '<div class="social-wrapper">');
    $block['content'][] =  theme('ra_alters_share_links', array('nid' => $node->nid));
    $block['content'][] =  array('#markup' => '</div>');
    $block['content'][] = array('#markup' => '<div class="social-all">' .
      l('All', 'modal/nojs/inspiration/list', array('attributes' => array('class'
        => array('modal' , 'ctools-modal-modal-popup-inspiration')))) .
      '</div>' );
  }

  return $render ? drupal_render($block) : $block;
}
