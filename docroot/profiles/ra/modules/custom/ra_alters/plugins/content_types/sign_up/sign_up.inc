<?php

define('RA_ALTERS_SIGNUP_TITLE', 'Sign up to get more!');
define('RA_ALTERS_SIGNUP_BUTTON_TEXT', 'Sign Up Now');
define('RA_ALTERS_SIGNUP_DESC', 'Our one-of-a-kind treatment tracking journal
Exclusive tips and advice
Treatment information');

$plugin = array(
  'title' => t('Signup Now'),
  'description' => t('Signup Now block.'),
  'single' => TRUE,
  'content_types' => array('sign_up'),
  'render callback' => 'ra_alters_signup_content_type_render',
  'defaults' => array(),
  'edit form' => 'ra_alters_signup_content_type_edit_form',
  'category' => array(t('Custom blocks'), 0),
);


/**
 * Render function.
 */
function ra_alters_signup_content_type_render($subtype, $conf) {
  $block = new stdClass();
  $block->title = '';
  $content_suffix = '';

  $classes = array('signup-block');
  
  $title = t($conf['title']);
  $desc_list = preg_split('/\r\n|[\r\n]/', t($conf['description']));
  $description = theme('item_list', array('items' => $desc_list));

  $button = l(t($conf['button_text']), 'sign-up');
  
  $animation_data_pars = '';
  if (isset($conf['use_animation']) && $conf['use_animation']) {
    $classes[] = 'animate';
    
    $default = isset($conf['background_x']) ? $conf['background_x'] : 0;
    $animation_data_pars .= " data-background-x='$default'";

    $default = isset($conf['bottom_y']) ? $conf['bottom_y'] : 10;
    $animation_data_pars .= " data-bottom-y='$default'";

    $default = isset($conf['fadeout_delay']) ? $conf['fadeout_delay'] : 0;
    $animation_data_pars .= " data-fadeout='$default'";
  }

  if (isset($conf['use_close_btn']) && $conf['use_close_btn']) {
    $content_suffix .= '<div class="signup-close-btn"><a href="#">Close</a></div>';
  }

  if ($conf['classes']) {
    $classes[] = $conf['classes'];
  }

  $class = implode(' ', $classes);
  $block->content = <<<HTML
<div class="$class" $animation_data_pars>
  $content_suffix
  <div class="signup-title">$title</div>
  <div class="signup-description">$description</div>
  <div class="signup-button">$button</div>
</div>
HTML;

  return $block;
}


/**
 * Edit form definition function.
 */
function ra_alters_signup_content_type_edit_form($form, &$form_state) {
  $conf = $form_state['conf'];

  $default = isset($conf['title']) ? $conf['title'] : RA_ALTERS_SIGNUP_TITLE;
  $form['title'] = array(
    '#type' => 'textfield',
    '#title' => t('Title'),
    '#required' => TRUE,
    '#default_value' => $default,
  );

  $default = isset($conf['description']) ? $conf['description'] : RA_ALTERS_SIGNUP_DESC;
  $form['description'] = array(
    '#type' => 'textarea',
    '#title' => t('Description'),
    '#required' => TRUE,
    '#default_value' => $default,
  );

  $default = isset($conf['button_text']) ? $conf['button_text'] : RA_ALTERS_SIGNUP_BUTTON_TEXT;
  $form['button_text'] = array(
    '#type' => 'textfield',
    '#title' => t('Button text'),
    '#required' => TRUE,
    '#default_value' => $default,
  );

  $form['use_close_btn'] = array(
    '#type' => 'checkbox',
    '#title' => t('Use close button'),
    '#default_value' => $conf['use_close_btn'],
  );

  $form['classes'] = array(
    '#type' => 'textfield',
    '#title' => t('Extra Classes'),
    '#default_value' => $conf['classes'],
  );

  $form['use_animation'] = array(
    '#type' => 'checkbox',
    '#title' => t('Use animation'),
    '#default_value' => $conf['use_animation'],
  );

  $form['animation_settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('Animation Settings'),
    '#states' => array(
      'visible' => array(
        ':input[name="use_animation"]' => array('checked' => TRUE),
      ),
    ),
  );

  $default = isset($conf['background_x']) ? $conf['background_x'] : 0;
  $form['animation_settings']['background_x'] = array(
    '#type' => 'textfield',
    '#title' => t('Background-x position'),
    '#default_value' => $default,
  );

  $default = isset($conf['bottom_y']) ? $conf['bottom_y'] : 10;
  $form['animation_settings']['bottom_y'] = array(
    '#type' => 'textfield',
    '#title' => t('Button bottom-y position'),
    '#default_value' => $default,
  );

  $default = isset($conf['fadeout_delay']) ? $conf['fadeout_delay'] : 0;
  $form['animation_settings']['fadeout_delay'] = array(
    '#type' => 'textfield',
    '#title' => t('Fadeout delay'),
    '#default_value' => $default,
  );

  return $form;
}


/**
 * Form submit function.
 */
function ra_alters_signup_content_type_edit_form_submit($form, &$form_state) {
  $fields = array(
    'title',
    'description',
    'button_text',
    'use_close_btn',
    'use_animation',
    'classes',
    'background_x',
    'bottom_y',
    'fadeout_delay',
  );

  foreach ($fields as $key) {
    $form_state['conf'][$key] = check_plain($form_state['values'][$key]);
  }
}
